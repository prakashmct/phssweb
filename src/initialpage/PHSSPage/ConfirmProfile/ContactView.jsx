/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import InputMask from 'react-input-mask';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
//import {Avatar_02,Avatar_05,Avatar_09,Avatar_10,Avatar_16 } from '../../../../../Entryfile/imagepath'
import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/style.css';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import Loader from '../../Loader';
import SystemHelpers from '../../Helpers/SystemHelper';

class ConatctView extends Component {
  constructor(props) {
    super(props);

    this.state = {
        errormsg :  '',
        user_role: [],
        all_data :this.props.all_data,
        staffContactID:this.props.staffContactID,

        // Display
        DisplayphssEmail : '',
        DisplaypersonalEmail : '',
        DisplaycellPhone : '',
        DisplayhomePhone : '',
        // Display

        // Edit
        EditPHHS_Email :  '',
        EditPersonal_Email :  '',
        EditCell_Phone :  '',
        EditCellPhoneNumberCountryCode : '',
        EditHome_Phone :  '',
        EditHomePhoneNumberCountryCode : '',
        // Edit

        role_contact_information_can :{}
    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
  }

  setPropState(key, value) {
    this.setState({ [key]: value });
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  push_func(){
    this.props.history.push("/");
    this.hideLoader();
  }

  SessionOut(){
    this.showLoader();
  
    localStorage.removeItem("token");
    localStorage.removeItem("contactId");
    localStorage.removeItem("eMailAddress");
    localStorage.removeItem("fullName");

    //this.ToastError('Session Timeout');
    
    setTimeout( () => {this.push_func()}, 5000);
    
    return null;
  }

  // toast hide show method
  ToastSuccess(msg){
      toast.success(msg, {
        position: "top-right",
        autoClose: process.env.API_TOAST_TIME,
        hideProgressBar: false,
        closeOnClick: false,
        pauseOnHover: false,
        draggable: false
      });
  }

  ToastError(msg){
      toast.error(msg, {
        position: "top-right",
        autoClose: process.env.API_TOAST_TIME,
        hideProgressBar: false,
        closeOnClick: false,
        pauseOnHover: false,
        draggable: false
      });
  }
  // toast hide show method

  componentDidMount() {
    console.log("ConatctView");

    /* Role Management */
    console.log('Role Store contact_information_can');
    console.log(this.props.contact_information_can);
    this.setState({ role_contact_information_can: this.props.contact_information_can });
    /* Role Management */

    console.log(this.state.all_data);

    // Display
    this.setState({ DisplayphssEmail :  this.state.all_data.phssEmail });
    this.setState({ DisplaypersonalEmail : this.state.all_data.personalEmail });
    this.setState({ DisplaycellPhone : this.state.all_data.cellPhone });
    this.setState({ DisplayhomePhone : this.state.all_data.homePhone });
    // Display

    // Edit
    this.setState({ EditPHHS_Email: this.state.all_data.phssEmail });
    this.setState({ EditPersonal_Email: this.state.all_data.personalEmail });
    
    if(this.state.all_data.cellPhone != '' && this.state.all_data.cellPhone != null){
      this.setState({ EditCell_Phone: this.state.all_data.cellPhone });
    }
    
    if(this.state.all_data.homePhone != '' && this.state.all_data.homePhone != null){
      this.setState({ EditHome_Phone: this.state.all_data.homePhone });
    }

    var CellCountryCode = this.state.all_data.cellPhone;
    if(CellCountryCode != null && CellCountryCode != ''){
      var CountryCodeCell = CellCountryCode.substring(0, CellCountryCode.length-10)
      this.setState({ EditCellPhoneNumberCountryCode: CountryCodeCell });
    }

    var HomeCountryCode = this.state.all_data.homePhone;
    if(HomeCountryCode != null && HomeCountryCode != ''){
      var CountryCodeHome = HomeCountryCode.substring(0, HomeCountryCode.length-10)
      this.setState({ EditHomePhoneNumberCountryCode: CountryCodeHome });
    }
    // Edit
    
  }

  //
  handleOnChangeCellMobile(value, data, event, formattedValue)
  {
    var phone = value;
    var str2 = "+";
    var country_code=data.dialCode;
    if(phone.indexOf(str2) != -1){
        console.log(str2 + " found");
    }else{
        value='+'+value;
        country_code="+"+data.dialCode;
    }
    console.log('Cell value ' + value);
    console.log('Cell formattedValue ' + formattedValue);
    console.log('Cell country_code ' + country_code);
    this.setState({ EditCell_Phone: value});
    this.setState({ EditCellPhoneNumberCountryCode : country_code });   
  }
  handleOnChangeHomeMobile(value, data, event, formattedValue)
  {
    var phone = value;
    var str2 = "+";
    var country_code=data.dialCode;
    if(phone.indexOf(str2) != -1){
        console.log(str2 + " found");
    }else{
        value='+'+value;
        country_code="+"+data.dialCode;
    }
    console.log('Home value ' + value);
    console.log('Home formattedValue ' + formattedValue);
    console.log('Home country_code ' + country_code);
    this.setState({ EditHome_Phone: value});
    this.setState({ EditHomePhoneNumberCountryCode : country_code });   
  }
  //

  //
  GetProfile(){
    console.log(localStorage.getItem("token"));
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserBasicInfoById?contactId='+this.state.staffContactID;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUserBasicInfoById");
        console.log(data);
        
        if (data.responseType === "1") 
        {
          localStorage.setItem("primaryLocationId", data.data.primaryLocationId);
          localStorage.setItem("isPayrollAdmin", data.data.isPayrollAdmin);
          
          this.setState({ all_data: data.data});
        }
        else
        {
          if(data.message == 'Authorization has been denied for this request.'){
            this.SessionOut();
          }else{
            this.ToastError(data.message);
          }
        }
        this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }
  //

  // Contact model Update api
  UpdateUserProfileTabContactModel_API = () => e => {  
      e.preventDefault();  
      
      let step1Errors = {};

      if(this.state["EditPersonal_Email"] == '' || this.state["EditPersonal_Email"] == null)
      {
        step1Errors["EditPersonal_Email"] = "Personal Email is mandatory";
      }
      else if (this.state["EditPersonal_Email"] !== "undefined" || this.state["EditPersonal_Email"] != '' || this.state["EditPersonal_Email"] != null)
      {
        let lastAtPos = this.state["EditPersonal_Email"].lastIndexOf('@');
        let lastDotPos = this.state["EditPersonal_Email"].lastIndexOf('.');

        if (!(lastAtPos < lastDotPos && lastAtPos > 0 && this.state["EditPersonal_Email"].indexOf('@@') == -1 && lastDotPos > 2 && (this.state["EditPersonal_Email"].length - lastDotPos) > 2)) 
        {
          step1Errors["EditPersonal_Email"] = "Email is not valid";
        }
      }
      
      var CellPhoneModel =this.state["EditCell_Phone"];
      var EditCellPhoneNumberCountryCode =this.state["EditCellPhoneNumberCountryCode"];
      if(this.state["EditCell_Phone"] == '' || this.state["EditCell_Phone"] == null || this.state["EditCell_Phone"] == '+'){
        step1Errors["EditCell_Phone"] = "Cell Phone is mandatory";
      }else{
        var total = CellPhoneModel.length - EditCellPhoneNumberCountryCode.length;
        if(total!=10)
        {
          step1Errors["EditCell_Phone"] = "Please Enter a Valid Cell Phone";
        }
      }
      
      
      var HomePhoneModel =this.state["EditHome_Phone"];
      var EditHomePhoneNumberCountryCode =this.state["EditHomePhoneNumberCountryCode"];
      if(this.state["EditHome_Phone"] == '' || this.state["EditHome_Phone"] == null || this.state["EditHome_Phone"] == '+')
      {
        step1Errors["EditCell_Phone"] = "Home Phone is mandatory";
      }
      else
      {
        var total = HomePhoneModel.length - EditHomePhoneNumberCountryCode.length;
        if(total!=10) 
        {
          step1Errors["EditHome_Phone"] = "Please Enter a Valid Home Phone";
        }
      }

      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      this.showLoader();

      let User_Basic_contact = {
        personalEmail: this.state["EditPersonal_Email"],
        cellPhone: this.state["EditCell_Phone"],
        homePhone: this.state["EditHome_Phone"]
      };

      let bodyarray = {};
      bodyarray["contactId"] = this.state.staffContactID;
      bodyarray["userBasicInfo"] = User_Basic_contact;
      
      var url=process.env.API_API_URL+'UpdateUserProfileContact';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
        if (data.responseType === "1") {
          this.GetProfile();  
          this.ToastSuccess('Contact updated successfully');
          $( "#close_btn_contact" ).trigger( "click" );
        }
        else{
          this.ToastError(data.responseMessge);
        }
        this.hideLoader();
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }
  // Contact model Update api

  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ errormsg: '' });

    // Edit
    this.setState({ EditPHHS_Email: this.state.all_data.phssEmail });
    this.setState({ EditPersonal_Email: this.state.all_data.personalEmail });
    this.setState({ EditCell_Phone: this.state.all_data.cellPhone });
    this.setState({ EditHome_Phone: this.state.all_data.homePhone });
    
    var CellCountryCode = this.state.all_data.cellPhone;
    if(CellCountryCode != null && CellCountryCode != ''){
      var CountryCodeCell = CellCountryCode.substring(0, CellCountryCode.length-10)
      this.setState({ EditCellPhoneNumberCountryCode: CountryCodeCell });
    }

    var HomeCountryCode = this.state.all_data.homePhone;
    if(HomeCountryCode != null && HomeCountryCode != ''){
      var CountryCodeHome = HomeCountryCode.substring(0, HomeCountryCode.length-10)
      this.setState({ EditHomePhoneNumberCountryCode: CountryCodeHome });
    }
    // Edit
  }

  render() {
     
    return (
        
        <div className="col-md-6 d-flex">
            {(this.state.loading) ? <Loader /> : null}
            <div className="card profile-box flex-fill">
              <div className="card-body">
                {this.state.role_contact_information_can.contact_information_can_update == true ? 
                  <h3 className="card-title">Contact <a href="#" className="edit-icon" data-toggle="modal" data-target="#ProfileTab_contact_modal"><i className="fa fa-pencil" /></a></h3>
                  : <h3 className="card-title">Contact <a href="#" className="phss-lock"><i className="fa fa-lock" /></a></h3>
                }
                <ul className="personal-info">
                  <li>
                    <div className="title">PHSS Email</div>
                    {this.state.DisplayphssEmail == '' || this.state.DisplayphssEmail == null? <div className="text hide-font">None</div> : <div className="text">{this.state.DisplayphssEmail}</div> }
                  </li>
                  <li>
                    <div className="title">Personal Email</div>
                    {this.state.DisplaypersonalEmail == '' || this.state.DisplaypersonalEmail == null? <div className="text hide-font">None</div> : <div className="text">{this.state.DisplaypersonalEmail}</div> }
                  </li>
                  <li>
                    <div className="title">Cell Phone </div>
                    {this.state.DisplaycellPhone == '' || this.state.DisplaycellPhone == null? <div className="text hide-font">None</div> : <div className="text">{this.state.DisplaycellPhone}</div> }
                  </li>
                  <li>
                    <div className="title">Home Phone </div>
                    {this.state.DisplayhomePhone == '' || this.state.DisplayhomePhone == null? <div className="text hide-font">None</div> : <div className="text">{this.state.DisplayhomePhone}</div> }
                  </li>

                </ul>
              </div>
            </div>

            {/* Staff Contact Information Modal */}
            <div id="ProfileTab_contact_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Contact Information</h5>
                    <button type="button" className="close" id="close_btn_contact" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()} >
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          {/* <h3 className="card-title">Primary Contact</h3> */}
                          <div className="row">
                            
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>PHSS Email</label>
                                <input className="form-control" type="text" value={this.state.EditPHHS_Email} readOnly = {true} />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Personal Email</label>
                                <input className="form-control" type="text" value={this.state.EditPersonal_Email} onChange={this.handleChange('EditPersonal_Email')} />
                                <span className="form-text error-font-color">{this.state.errormsg["EditPersonal_Email"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Cell Phone <span className="text-danger">*</span></label>
                                <PhoneInput
                                  inputClass='form-control'
                                  inputStyle= {{'width': '100%'}}
                                  country={"ca"}
                                  onlyCountries={['ca', 'in' ,'us']}
                                  value={this.state.EditCell_Phone}
                                  onChange={this.handleOnChangeCellMobile.bind(this)}
                                />
                                <span className="form-text error-font-color">{this.state.errormsg["EditCell_Phone"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Home Phone </label>
                                <PhoneInput
                                  inputClass='form-control'
                                  inputStyle= {{'width': '100%'}}
                                  country={"ca"}
                                  onlyCountries={['ca', 'in' ,'us']}
                                  value={this.state.EditHome_Phone}
                                  onChange={this.handleOnChangeHomeMobile.bind(this)}
                                />
                                <span className="form-text error-font-color">{this.state.errormsg["EditHome_Phone"]}</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="submit-section">
                        <button className="btn btn-primary submit-btn" type="submit" onClick={this.UpdateUserProfileTabContactModel_API({})} >Submit</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* //Staff Contact Information Modal */}
          </div>
        
      );
   }
}

export default ConatctView;
