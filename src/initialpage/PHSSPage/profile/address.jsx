/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import Loader from '../../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import moment from 'moment';
import SystemHelpers from '../../Helpers/SystemHelper';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';
//table

import SelectDropdown from 'react-dropdown-select';

class Address extends Component {
  constructor(props) {
    super(props);

    this.state = {

        // Pagination 
        totalCount : 0,
        pageSize : 5,
        currentPage : 1,
        totalPages : 0,
        previousPage : false,
        nextPage : false,
        searchText : '',
        pagingData : {},
        TempsearchText:'',

        sortColumn : 'AddressType',
        SortType : true,
        IsSortingEnabled : true,
        // Pagination

        errormsg :  '',
        staffContactID:this.props.staffContactID,
        userAddressType:[],
        user_role: [],
        ListGrid: [],
        ListCountry: [],
        ListProvince:[],
        ListCity:[],

        // add model
        AddAddressType:'',
        AddStreet:'',
        AddUnit:'',
        AddStreetName:'',
        AddCountry:'',
        AddProvince:'',
        AddCity:'',
        AddPostalCode:'',
        AddAddressLine2:'',
        // add model

        // edit model
        EditAddressId:'',
        EditAddressType:'',
        EditStreet:'',
        EditUnit:'',
        EditStreetName:'',
        EditCountry:'',
        EditProvince:'',
        EditCity:'',
        EditPostalCode:'',
        EditAddressLine2:'',
        role_address_can : {},
        // edit model

        isDelete : false,

        header_data : [],
        SelectFCityList : [],
        staffContactFullname : localStorage.getItem('fullName')
    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
    
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  handleChange = input => e => {
    this.setState({ [input]: e.target.value.replace(/^\s+/g, '') });
    // console.log(input);
    if (this.state[input] != '') {
      
        delete this.state.errormsg[input];
    }

    // Add time
    if([input]=="AddCountry")
    {
      this.setState({ ListProvince: [] });
      this.setState({ ListCity: [] });

      this.GetProvince(e.target.value);
      this.setState({ AddProvince: '' });
      this.setState({ AddCity: '' });
    }

    if([input]=="AddProvince")
    {
      this.setState({ ListCity: [] });

      this.GetCity(e.target.value);
      this.setState({ AddCity: '' });
    }
    // Add time

    // Edit time
    if([input]=="EditCountry")
    {
      this.setState({ ListProvince: [] });
      this.setState({ ListCity: [] });

      this.GetProvince(e.target.value);
      this.setState({ EditProvince: '' });
      this.setState({ EditCity: '' });
    }

    if([input]=="EditProvince")
    {
      this.setState({ ListCity: [] });

      this.GetCity(e.target.value);
      this.setState({ EditCity: '' });
    } 
    // Edit time
    
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

    

  setPropState(key, value) {
      this.setState({ [key]: value });
  }
  // Loader hide show method
  GetCountry(){
    this.showLoader();
    var url=process.env.API_API_URL+'GetCountry';
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        //'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        // console.log("responseJson GetCountry");
        // console.log(data);
        // console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            if(data.data != null){
               this.setState({ ListCountry: data.data});
            } 
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('GetCountry error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }

  GetProvince(id){
    this.showLoader();
    var url=process.env.API_API_URL+'GetProvince?id='+id;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        //'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        // console.log("responseJson GetProvince");
        // console.log(data);
        // console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            if(data.data != null){
               this.setState({ ListProvince: data.data});
            } 
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('GetProvince error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }

  GetCity(id){
    this.showLoader();
    var url=process.env.API_API_URL+'GetCity?id='+id;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        //'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        // console.log("responseJson GetCity");
        // console.log(data);
        // console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            if(data.data != null){
              
              this.setState({ ListCity: data.data});
              this.setState({ SelectFCityList: this.FuncFCityList(data.data) });
            } 
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('GetCity error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }

  FuncFCityList(City)
  {
    
    // console.log('fun FuncFCityList');
    // console.log(City);
    // console.log(this.state.EditCity);
    var City_lenght = City.length;
    let dataArray = [];
   
    if(City_lenght > 0)
    {
      
      for (var z = 0; z < City_lenght; z++) {
        var tempdataArray = [];
        //SelectFCityList
        
        if(this.state.EditCity == City[z].cityId)
        {
          dataArray.push(City[z]);
        }
        
      }
    }
    // console.log(dataArray);
    return dataArray;
    
  }

  FilterCitySelect (City){
    
    // console.log('FilterCitySelect');
    // console.log(City);
    if(City.length > 0)
    {
      this.setState({ AddCity: City[0].cityId })
    }

  }

  FilterCitySelectEdit (City){
    
    // console.log('FilterCitySelectEdit');
    // console.log(City);
    if(City.length > 0)
    {
      this.setState({ EditCity: City[0].cityId })
    }
  }

  componentDidMount() {

    /* Role Management */
     // console.log('Role Store address_can');
     /*var getrole = SystemHelpers.GetRole();
     let address_can = getrole.address_can;
     this.setState({ role_address_can: address_can });
     // console.log(address_can);*/
     
     // console.log(this.props.address_can);
     let address_can = this.props.address_can;
     this.setState({ role_address_can: this.props.address_can });
    /* Role Management */

    // Delete Permison
    // Delete Permison

    this.GetCountry();
    this.GetUserAddress(this.state.currentPage,this.state.pageSize,this.state.searchText);
  }

  Edit_Update_Btn_Func(addresslist){
    let return_push = [];
    // console.log('edit button address');
    // console.log(addresslist);
    if(this.state.role_address_can.address_can_update == true || this.state.role_address_can.address_can_delete == true){
      
      let Edit_push = [];
      if(this.state.role_address_can.address_can_update == true && addresslist.isDelete == false){
        Edit_push.push(
          <a href="#" onClick={this.EditRecord(addresslist)} className="dropdown-item" data-toggle="modal" data-target="#ProfileTab_address_info_edit_modal"><i className="fa fa-pencil m-r-5" ></i> Edit</a>
        );
      }

      let Delete_push = [];
      if(this.state.role_address_can.address_can_delete == true){
        if(addresslist.isDelete == false)
        {
          Delete_push.push( 
            <a href="#" onClick={this.DeleteRecordEditRecord(addresslist)}  className="dropdown-item"  data-toggle="modal" data-target="#ProfileTab_address_info_delete_modal" ><i className="fa fa-trash-o m-r-5" ></i> Inactive</a>
          );
        }
        else
        {
          Delete_push.push( 
            <a href="#" onClick={this.DeleteRecordEditRecord(addresslist)}  className="dropdown-item"  data-toggle="modal" data-target="#ProfileTab_address_info_delete_modal" ><i className="fa fa-trash-o m-r-5" ></i> Active</a>
          );
        }
        
      }
      
      return_push.push(
        <div className="dropdown dropdown-action">
          <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
          <div className="dropdown-menu dropdown-menu-right">
            {Edit_push}
            {Delete_push}
          </div>
        </div>
      );
    }
    return return_push;
  }

  EditRecord = (record) => e => {
    e.preventDefault();

    this.setState({ errormsg: '' });

    this.setState({ EditAddressId: record.addressId });
    this.setState({ EditAddressType: record.addressType });
    this.setState({ EditStreet: record.street });
    this.setState({ EditUnit: record.unit });
    this.setState({ EditStreetName: record.streetName });
    this.setState({ EditCountry: record.countryId });
    this.setState({ EditProvince: record.provinceId });
    this.setState({ EditCity: record.cityId });
    this.setState({ EditPostalCode: record.postalCode });
    this.setState({ EditAddressLine2: record.streetName2 });

    this.GetProvince(record.countryId);
    this.GetCity(record.provinceId);

  }

  DeleteRecordEditRecord = (record) => e => {
    e.preventDefault();

    this.setState({ EditAddressId: record.addressId });
    this.setState({ isDelete: record.isDelete });
  }

  TableHeaderDesign()
  {
      if(this.state.role_address_can.address_can_delete == true || this.state.role_address_can.address_can_delete == "true")
      {
        let columns = [];
        columns.push(<tr>
            <th>Address Type</th>
            <th>Street #</th>
            <th>Unit #</th>
            <th>Street Name</th>
            <th>Address Line 2</th>
            <th>Postal Code</th>
            <th>City</th>
            <th>Province</th>
            <th>Country</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        );
        return columns;
      }
      else
      {
        let columns = [];
        columns.push(<tr>
            <th>Address Type</th>
            <th>Street #</th>
            <th>Unit #</th>
            <th>Street Name</th>
            <th>Address Line 2</th>
            <th>Postal Code</th>
            <th>City</th>
            <th>Province</th>
            <th>Country</th>
            <th>Action</th>
          </tr>
        );
        return columns; 
      }
  }

  GetUserAddress(currentPage,pageSize,searchText){

    /* Role Management */
     var getrole = SystemHelpers.GetRole();
     let canDelete = getrole.address_can.address_can_delete;
    /* Role Management */

    this.showLoader();
    
    this.setState({ ListGrid : [] });

    // Pagination
    let bodyarray = {};
    bodyarray["currentPage"] = 1;
    bodyarray["nextPage"] = false;
    bodyarray["pageSize"] = 5;
    bodyarray["previousPage"] = false;
    bodyarray["totalCount"] = 0;
    bodyarray["totalPages"] = 0;
    
    this.setState({ pagingData : bodyarray });

    this.setState({ currentPage: currentPage });
    this.setState({ pageSize: pageSize });

    var sort_Column = this.state.sortColumn;
    var Sort_Type = this.state.SortType;
    
    var IsSortingEnabled = true;

    var url_paging_para = '&pageNumber='+currentPage+'&pageSize='+pageSize+'&searchText='+searchText+'&sortColumn='+sort_Column+'&SortType='+Sort_Type+'&IsSortingEnabled='+IsSortingEnabled;
    // Pagination
    

    var url=process.env.API_API_URL+'GetUserAddress?contactId='+this.state.staffContactID+'&canDelete='+canDelete+url_paging_para;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        // console.log("responseJson GetUserAddress");
        // console.log(data);
        // console.log(data.data.userRole);
        // console.log(data.data.userAddressType);
        // debugger;
        if (data.responseType == "1") {
            // Profile & Contact
            //this.setState({ ListGrid: data.data.userAddressInfos });
            this.setState({ header_data: this.TableHeaderDesign() });
            this.setState({ ListGrid: this.rowData(data.data.userAddressInfos) })
            this.setState({ userAddressType: data.data.userAddressType });
            // console.log('userAddressType');
            // console.log(this.state.userAddressType);
            // console.log('userAddressType');
            this.setState({ pagingData: data.pagingData });
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      // console.log('123');
      console.log('GetUserAddress error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }
  
  ClearRecord = ()=> e => {
    e.preventDefault();


    $( ".react-dropdown-select-clear" ).trigger( "click" );

    
    
    
    this.setState({ AddAddressType: 0 });
    this.setState({ AddStreet: '' });
    this.setState({ AddUnit: '' });
    this.setState({ AddStreetName: '' });
    this.setState({ AddPostalCode: '' });
    this.setState({ AddCity: "" });
    this.setState({ AddProvince: 0 });
    this.setState({ AddCountry: 0 });
    this.setState({ AddAddressLine2: '' });

    this.setState({ ListProvince: [] });
    this.setState({ ListCity: [] });

    
    this.setState({ errormsg: '' });
  }

  AddRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};

    
    
    if (this.state["AddAddressType"] =='') {
      step1Errors["AddAddressType"] = "Address Type is mandatory";
    }

    

    /*if (this.state["AddUnit"] == '') {
      step1Errors["AddUnit"] = "Unit is mandatory";
    }*/

   
    if (this.state["AddStreetName"] == '') {
      step1Errors["AddStreetName"] = "Street Name is mandatory";
    }

    if (this.state["AddCountry"]  == '') {
      step1Errors["AddCountry"] = "Country is mandatory";
    }

    if (this.state["AddProvince"] == '') {
      step1Errors["AddProvince"] = "Province is mandatory";
    }

    if (this.state["AddCity"] == '') {
      step1Errors["AddCity"] = "City is mandatory";
    }

    if (this.state["AddPostalCode"] == '') {
      step1Errors["AddPostalCode"] = "Postal Code is mandatory";
    }

    if (this.state["AddStatus"] == '') {
      step1Errors["AddStatus"] = "Status is mandatory";
    }
    

    // console.log(step1Errors);

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }

    //return false;

    this.showLoader();
    
    let ArrayJson = {
          addressType: this.state["AddAddressType"],
          street: this.state["AddStreet"],
          unit: this.state["AddUnit"],
          streetName: this.state["AddStreetName"],
          postalCode: this.state["AddPostalCode"],
          cityId: this.state["AddCity"],
          provinceId: this.state["AddProvince"],
          countryId: this.state["AddCountry"],
          streetName2: this.state["AddAddressLine2"]
    };
     
    let bodyarray = {};
    bodyarray["contactId"] = this.state.staffContactID;
    bodyarray["userAddressInfo"] = ArrayJson;

    // console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'CreateUserAddress';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        // console.log("responseJson CreateUserAddress");
        // console.log(data);
        // console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');

            this.setState({ AddAddressType: 0 });
            this.setState({ AddStreet: '' });
            this.setState({ AddUnit: '' });
            this.setState({ AddStreetName: '' });
            this.setState({ AddPostalCode: '' });
            this.setState({ AddCity: 0 });
            this.setState({ AddProvince: 0 });
            this.setState({ AddCountry: 0 });
            this.setState({ AddAddressLine2: '' });

            this.setState({ ListProvince: [] });
            this.setState({ ListCity: [] });

            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".close" ).trigger( "click" );
            this.GetUserAddress(this.state.currentPage,this.state.pageSize,this.state.searchText);
               
        }else if (data.responseType === "2") {
          //SystemHelpers.ToastSuccess(data.responseMessge);  
          SystemHelpers.ToastError(data.responseMessge);
          //$( ".close" ).trigger( "click" ); 
          //this.GetEmergencyContact();
        }
        else{
            SystemHelpers.ToastError(data.message  );
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('CreateUserAddress error');
      console.log(error);
      this.props.history.push("/error-500");
    });
    return false;
    
  }

  UpdateRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};
    
    if (this.state["EditAddressType"] =='' || this.state["EditAddressType"] =='-') {
      step1Errors["EditAddressType"] = "Address Type is mandatory";
    }

 
    /*if (this.state["EditStreet"] == '') {
      step1Errors["EditStreet"] = "Street is mandatory";
    }*/

    /*if (this.state["EditUnit"] == '') {
      step1Errors["EditUnit"] = "Unit is mandatory";
    }*/

   
    if (this.state["EditStreetName"] == '') {
      step1Errors["EditStreetName"] = "Street Name is mandatory";
    }

    if (this.state["EditCountry"]  == '') {
      step1Errors["EditCountry"] = "Country is mandatory";
    }

    if (this.state["EditProvince"] == '') {
      step1Errors["EditProvince"] = "Province is mandatory";
    }

    if (this.state["EditCity"] == '') {
      step1Errors["EditCity"] = "City is mandatory";
    }

    if (this.state["EditPostalCode"] == '') {
      step1Errors["EditPostalCode"] = "Postal Code is mandatory";
    }

    if (this.state["EditStatus"] == '') {
      step1Errors["EditStatus"] = "Status is mandatory";
    }
    

    // console.log(step1Errors);

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }

    //return false;

    this.showLoader();
    
    let ArrayJson = {
          addressId: this.state["EditAddressId"],
          addressType: this.state["EditAddressType"],
          street: this.state["EditStreet"],
          unit: this.state["EditUnit"],
          streetName: this.state["EditStreetName"],
          postalCode: this.state["EditPostalCode"],
          cityId: this.state["EditCity"],
          provinceId: this.state["EditProvince"],
          countryId: this.state["EditCountry"],
          streetName2: this.state["EditAddressLine2"],
          contactId : this.state.staffContactID
    };
     
    let bodyarray = {};
    bodyarray["contactId"] = this.state.staffContactID;
    bodyarray["userAddressInfo"] = ArrayJson;
    bodyarray["userName"] = this.state.staffContactFullname;

    // console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'UpdateUserAddress';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        // console.log("responseJson UpdateUserAddress");
        // console.log(data);
        // console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');
            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".close" ).trigger( "click" );
            this.GetUserAddress(this.state.currentPage,this.state.pageSize,this.state.searchText);
               
        }else if (data.responseType === "2") {
          //SystemHelpers.ToastSuccess(data.responseMessge);  
          SystemHelpers.ToastError(data.responseMessge );
          //$( ".close" ).trigger( "click" ); 
          //this.GetEmergencyContact();
        }
        else{
            SystemHelpers.ToastError(data.message  );
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('UpdateUserAddress error');
      console.log(error);
      this.props.history.push("/error-500");
    });
    return false;
    
  }

  DeleteRecord = () => e => {
    e.preventDefault();

    var isdelete = '';
    if(this.state.isDelete== true)
    {
      isdelete = false;
    }
    else
    {
      isdelete = true;
    }

    this.showLoader();
    var url=process.env.API_API_URL+'DeleteUserAddress?addressId='+this.state.EditAddressId+'&isDelete='+isdelete+'&userName='+this.state.staffContactFullname;
    fetch(url, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        // console.log("responseJson DeleteUserAddress");
        // console.log(data);
        // console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.GetUserAddress(this.state.currentPage,this.state.pageSize,this.state.searchText);
            this.hideLoader();
        }else if (data.responseType == "2" || data.responseType == "3") {
            SystemHelpers.ToastError(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.hideLoader();
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              this.hideLoader();
              $( ".cancel-btn" ).trigger( "click" );
        }
        
        
    })
    .catch(error => {
      console.log('DeleteUserAddress error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }

  rowData(ListGrid) {
    // console.log('Row Address');
    // console.log(ListGrid);
    // console.log(ListGrid.length);

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    let canDelete = getrole.address_can.address_can_delete;
    /* Role Management */
    
    var ListGrid_length = ListGrid.length;
      let dataArray = [];
      var i=1;
      if(ListGrid_length > 0) {

        for (var z = 0; z < ListGrid_length; z++) {
          

          var tempdataArray = [];
          //tempdataArray.rownum = i;
          var status = "";
          if(canDelete == true)
          {
            if(ListGrid[z].isDelete == true){
              status = <div><span class="badge bg-inverse-warning">Inactive</span></div>;
            }else{
              status = <div><span class="badge bg-inverse-success">Active</span></div>;
            }
          }

          tempdataArray.push(<tr key={z}>
            <td>{ListGrid[z].addressType}</td>
            <td>{ListGrid[z].street}</td>
            <td>{ListGrid[z].unit}</td>
            <td>{ListGrid[z].streetName}</td>
            <td>{ListGrid[z].streetName2}</td>
            <td>{ListGrid[z].postalCode}</td>
            <td>{ListGrid[z].city}</td>
            <td>{ListGrid[z].province}</td>
            <td>{ListGrid[z].country}</td>
            <td>{status}</td>
            <td>{this.Edit_Update_Btn_Func(ListGrid[z])}</td>
          </tr>); 
         

          dataArray.push(tempdataArray);

          i++;
        }
        
      }
      
      return dataArray;
  }

  // Pagination Design
  PaginationDesign ()
  {
    let PageOutput = [];
    // console.log('pagination');
    // console.log(this.state.pagingData);
    
    if(this.state.pagingData !="" && this.state.pagingData !="undefined")
    {
      var Page_Count = this.state.pagingData.totalPages;
      //alert(this.state.pagingData.currentPage);
      // console.log('page count = ' + Page_Count);
      /* pagination count */

      var Page_Start=1;
      var Page_End=1;

      if(this.state.pagingData.currentPage == 1){
        Page_Start=1;

        if(Page_Count <= 10){
          Page_End=Page_Count;
        }else{
          Page_End=10;
        }
      }else{
        if(this.state.pagingData.currentPage < 5){
          Page_Start=1;
          Page_End=Page_Count;
          //Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
          // console.log("Page_End 1 "+ Page_End);
        }else{
          Page_Start=parseInt(this.state.pagingData.currentPage) - parseInt(4);
          Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
          // console.log("Page_End 2 "+ Page_End);
          if(Page_End > Page_Count){
            Page_End=Page_Count;
            // console.log("Page_End 3 "+ Page_End);
          }
        }
      }

      let Page = [];
      var i = 1;
      for (var z=Page_Start; z <= Page_End ; z++)
      {
        if(z==this.state.pagingData.currentPage)
        {
          Page.push(<li className="page-item active pk-active">
            <a className="page-link pk-active" id={z} href="#" onClick={this.PageGetGridData}>{z}<span className="sr-only">(current)</span></a>
          </li>);
        }
        else
        {
          Page.push(<li className="page-item"><a className="page-link" id={z} href="#" onClick={this.PageGetGridData} >{z}</a></li>);
        }
        i++;
      }

      let PagePrev = [];

      if(this.state.pagingData.currentPage == 1){
        PagePrev.push(<li className="page-item disabled">
          <a className="page-link" href="#">Previous</a>
        </li>);
      }else{
        PagePrev.push(<li className="page-item">
          <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)-parseInt(1)} tabIndex={-1} onClick={this.PageGetGridData}>Previous</a>
        </li>);
      }

      let PageNext = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageNext.push(<li className="page-item disabled">
          <a className="page-link" href="#">Next</a>
        </li>);
      }else{
        PageNext.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)+parseInt(1)} onClick={this.PageGetGridData}>Next</a>
          </li>
        );
      }

      let PageLast = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageLast.push(<li className="page-item disabled">
          <a className="page-link" href="#">Last</a>
        </li>);
      }else{
        PageLast.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(Page_Count)} onClick={this.PageGetGridData}>Last</a>
          </li>
        );
      }
      
      PageOutput.push(<section className="comp-section" id="comp_pagination">
                        <div className="pagination-box">
                          <div>
                            <ul className="pagination">
                              
                              {PagePrev}
                              {Page}
                              {PageNext}
                              {PageLast}
                              
                            </ul>
                          </div>
                        </div>
                      </section>);
    }
    
    return PageOutput;
  }

  PageGetGridData = e => {

    e.preventDefault();
    let current_page= e.target.id;
    this.GetUserAddress(current_page,this.state.pageSize,this.state.searchText)
  }

  SearchGridData = e => {
    this.setState({ pageSize: this.state.TempsearchText });
    this.GetUserAddress(1,this.state.pageSize,this.state.TempsearchText);
  }
  // Pagination Design

  render() {
      
     
      return (
      <div className="row">
        {/* Toast & Loder method use */}
            
        {(this.state.loading) ? <Loader /> : null} 
        {/* Toast & Loder method use */}
        <div className="col-md-12 d-flex">
          <div className="card profile-box flex-fill">
            <div className="card-body">
              {this.state.role_address_can.address_can_create == true ?
                <h3 className="card-title">Address <a href="#" className="edit-icon" data-toggle="modal" data-target="#ProfileTab_address_info_add_modal"><i className="fa fa-plus" /></a></h3>
                : <h3 className="card-title">Address <a href="#" className="phss-lock"><i className="fa fa-lock" /></a></h3>
              }

              {/* Page Per Record and serach design*/}
                <div className="row filter-row">
                  <div className="col-sm-6 col-md-2"> 
                    <div className="form-group form-focus select-focus">
                      <select className="form-control floating" value={this.state.pageSize}  onChange={this.handleChange('pageSize')}> 
                        <option value="5">5/Page</option>
                        <option value="10">10/Page</option>
                        <option value="50">50/Page</option>
                        <option value="100">100/Page</option>
                      </select>
                      <label className="focus-label">Per Page</label>
                    </div>
                  </div>
                  
                  <div className="col-sm-6 col-md-3">
                    <div className="form-group form-focus focused">
                      <label className="focus-label">Sorting</label>
                      <select className="form-control floating" id="sortColumn" value={this.state.sortColumn} onChange={this.handleChange('sortColumn')}> 
                        {/*<option value="">-</option>*/}
                        <option value="AddressType">Address Type</option>
                      </select>
                    </div>
                  </div> 

                  <div className="col-sm-6 col-md-2">
                    <div className="form-group form-focus focused">
                      <label className="focus-label">Sorting Order</label>
                      <select className="form-control floating" id="SortTypeId" value={this.state.SortType} onChange={this.handleChange('SortType')}> 
                        {/*<option value="">-</option>*/}
                        <option value="false">Ascending</option>
                        <option value="true">Descending</option>
                      </select>
                    </div>
                  </div> 

                  <div className="col-sm-6 col-md-3">  
                    <div className="form-group form-focus focused">
                      <input className="form-control floating" type="text" value={this.state.TempsearchText}  onChange={this.handleChange('TempsearchText')} placeholder="Search by Street Name, Address Line" />
                      <label className="focus-label">Search</label>
                    </div>
                  </div>

                  <div className="col-sm-6 col-md-2">  
                    <a href="#" className="btn btn-success btn-block" onClick={this.SearchGridData}> Search </a>  
                  </div> 
                </div>
              {/* Page Per Record and serach design*/}

                <div className="row">
                  <div className="col-md-12">
                    <div className="table-responsive">
                      
                      <table className="table table-striped custom-table mb-0 datatable">
                        <thead>
                          { this.state.ListGrid.length > 0 ? this.state.header_data : null}
                        </thead>
                        <tbody>
                          
                          {this.state.ListGrid.length > 0 ? this.state.ListGrid : null}
                        </tbody>
                       </table> 

                      {this.PaginationDesign()}

                    </div>
                  </div>
                </div>

            </div>
          </div>
        </div>

        {/* Address Informations Modal */}
          <div id="ProfileTab_address_info_add_modal" className="modal custom-modal fade" role="dialog">
            <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title">Address Information</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()}>
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div className="modal-body">
                  <form>
                    <div className="card">
                      <div className="card-body">
                        <div className="row">
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Address Type <span className="text-danger">*</span></label>
                              <select className="form-control" value={this.state.AddAddressType} onChange={this.handleChange('AddAddressType')}>
                                <option>-</option>
                                {this.state.userAddressType.map(( listValue, index ) => {
                                  return (
                                    <option key={index} value={listValue.name}>{listValue.name}</option>
                                  );
                                })}
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["AddAddressType"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Street # </label>
                              <input pattern=".{2,}" className="form-control" type="text" value={this.state.AddStreet} onChange={this.handleChange('AddStreet')}/>
                              <span className="form-text error-font-color">{this.state.errormsg["AddStreet"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Unit # </label>
                              <input className="form-control" type="text" value={this.state.AddUnit} onChange={this.handleChange('AddUnit')}/>
                              <span className="form-text error-font-color">{this.state.errormsg["AddUnit"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Street Name <span className="text-danger">*</span></label>
                              <input className="form-control" type="text" value={this.state.AddStreetName} onChange={this.handleChange('AddStreetName')}/>
                              <span className="form-text error-font-color">{this.state.errormsg["AddStreetName"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Address Line 2 </label>
                              <input className="form-control" type="text" value={this.state.AddAddressLine2} onChange={this.handleChange('AddAddressLine2')}/>
                              <span className="form-text error-font-color">{this.state.errormsg["AddAddressLine2"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Country <span className="text-danger">*</span></label>
                              <select className="form-control" value={this.state.AddCountry} onChange={this.handleChange('AddCountry')}>
                                <option value="">-</option>
                                {this.state.ListCountry.map(( listValue, index ) => {
                                  return (
                                    <option key={index} value={listValue.countryId}>{listValue.countryName}</option>
                                  );
                                })}
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["AddCountry"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Province <span className="text-danger">*</span></label>
                              <select className="form-control" value={this.state.AddProvince} onChange={this.handleChange('AddProvince')}>
                                <option value="">-</option>
                                {this.state.ListProvince.map(( listValue, index ) => {
                                  return (
                                    <option key={index} value={listValue.provinceId}>{listValue.provinceName}</option>
                                  );
                                })}
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["AddProvince"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>City <span className="text-danger">*</span></label>
                              <SelectDropdown
                                options={this.state.ListCity}
                                valueField="cityId"
                                labelField="cityName"
                                searchBy="cityName"
                                className="form-control"
                                placeholder="Search City"
                                id="cityId"
                               
                                values={this.state.SelectFCityList}
                                onChange={(values) => this.FilterCitySelect(values)}
                                
                                clearable="true"
                                

                              />
                              {/*<select className="form-control" value={this.state.AddCity} onChange={this.handleChange('AddCity')}>
                                <option value="">-</option>
                                {this.state.ListCity.map(( listValue, index ) => {
                                  return (
                                    <option key={index} value={listValue.cityId}>{listValue.cityName}</option>
                                  );
                                })}
                              </select>*/}
                              <span className="form-text error-font-color">{this.state.errormsg["AddCity"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Postal Code <span className="text-danger">*</span></label>
                              <input className="form-control" type="text" value="123-456-7890" value={this.state.AddPostalCode} onChange={this.handleChange('AddPostalCode')}/>
                              <span className="form-text error-font-color">{this.state.errormsg["AddPostalCode"]}</span>
                            </div>
                          </div>
                          
                          
                        </div>
                        <div className="submit-section">
                          <button className="btn btn-primary submit-btn" onClick={this.AddRecord()}>Submit</button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        {/* //Address Informations Modal */}

        {/* Update Address Informations Modal */}
          <div id="ProfileTab_address_info_edit_modal" className="modal custom-modal fade" role="dialog">
            <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title">Address Information</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()}>
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div className="modal-body">
                  <form>
                    <div className="card">
                      <div className="card-body">
                        <div className="row">
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Address Type <span className="text-danger">*</span></label>
                              <select className="form-control" value={this.state.EditAddressType} onChange={this.handleChange('EditAddressType')}>
                                <option>-</option>
                                {this.state.userAddressType.map(( listValue, index ) => {
                                  return (
                                    <option key={index} value={listValue.name}>{listValue.name}</option>
                                  );
                                })}
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["EditAddressType"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Street # </label>
                              <input className="form-control" type="text" value={this.state.EditStreet} onChange={this.handleChange('EditStreet')}/>
                              <span className="form-text error-font-color">{this.state.errormsg["EditStreet"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Unit # </label>
                              <input className="form-control" type="text" value={this.state.EditUnit} onChange={this.handleChange('EditUnit')}/>
                              <span className="form-text error-font-color">{this.state.errormsg["EditUnit"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Street Name <span className="text-danger">*</span></label>
                              <input className="form-control" type="text" value={this.state.EditStreetName} onChange={this.handleChange('EditStreetName')}/>
                              <span className="form-text error-font-color">{this.state.errormsg["EditStreetName"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Address Line 2 </label>
                              <input className="form-control" type="text" value={this.state.EditAddressLine2} onChange={this.handleChange('EditAddressLine2')}/>
                              <span className="form-text error-font-color">{this.state.errormsg["EditAddressLine2"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Country <span className="text-danger">*</span></label>
                              <select className="form-control" value={this.state.EditCountry} onChange={this.handleChange('EditCountry')}>
                                <option value="">-</option>
                                {this.state.ListCountry.map(( listValue, index ) => {
                                  return (
                                    <option key={index} value={listValue.countryId}>{listValue.countryName}</option>
                                  );
                                })}
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["EditCountry"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Province <span className="text-danger">*</span></label>
                              <select className="form-control" value={this.state.EditProvince} onChange={this.handleChange('EditProvince')}>
                                <option value="">-</option>
                                {this.state.ListProvince.map(( listValue, index ) => {
                                  return (
                                    <option key={index} value={listValue.provinceId}>{listValue.provinceName}</option>
                                  );
                                })}
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["EditProvince"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>City <span className="text-danger">*</span></label>
                              
                              <SelectDropdown
                                options={this.state.ListCity}
                                valueField="cityId"
                                labelField="cityName"
                                searchBy="cityName"
                                className="form-control"
                                placeholder="Search City"
                                id="cityId"
                               
                                values={this.state.SelectFCityList}
                                onChange={(values) => this.FilterCitySelectEdit(values)}
                                
                              />

                              {/*<select className="form-control" value={this.state.EditCity} onChange={this.handleChange('EditCity')}>
                                <option value="">-</option>
                                {this.state.ListCity.map(( listValue, index ) => {
                                  return (
                                    <option key={index} value={listValue.cityId}>{listValue.cityName}</option>
                                  );
                                })}
                              </select>*/}
                              <span className="form-text error-font-color">{this.state.errormsg["EditCity"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Postal Code <span className="text-danger">*</span></label>
                              <input className="form-control" type="text" value="123-456-7890" value={this.state.EditPostalCode} onChange={this.handleChange('EditPostalCode')}/>
                              <span className="form-text error-font-color">{this.state.errormsg["EditPostalCode"]}</span>
                            </div>
                          </div>
                          
                          
                        </div>
                        <div className="submit-section">
                          <button className="btn btn-primary submit-btn" onClick={this.UpdateRecord()}>Update</button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        {/* //Update Address Informations Modal */}

        {/* Delete Trained Locations  Modal */}
          <div className="modal custom-modal fade" id="ProfileTab_address_info_delete_modal" role="dialog">
            <div className="modal-dialog modal-dialog-centered">
              <div className="modal-content">
                <div className="modal-body">
                  <div className="form-header">
                    <h3>Address Information</h3>
                    <p>Are you sure you want to mark address as {this.state.isDelete == true ? 'Active' : 'Inactive' } ?</p>
                  </div>
                  <div className="modal-btn delete-action">
                    <div className="row">
                      <div className="col-6">
                        <a  onClick={this.DeleteRecord()} className="btn btn-primary continue-btn">{this.state.isDelete == true ? 'Active' : 'Inactive' }</a>
                      </div>
                      <div className="col-6">
                        <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">Cancel</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        {/* /Delete Trained Locations Modal */}

      </div>
      );
   }
}

export default Address;
