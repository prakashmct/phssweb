/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import moment from 'moment';

import Loader from '../../../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../../../Helpers/SystemHelper';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';
//table

import Datetime from "react-datetime";

class EmploymentHistory extends Component {
  constructor(props) {
    super(props);

    this.state = {

        // Pagination 
        totalCount : 0,
        pageSize : 5,
        currentPage : 1,
        totalPages : 0,
        previousPage : false,
        nextPage : false,
        searchText : '',
        pagingData : {},
        TempsearchText:'',

        sortColumn : 'StartDate',
        SortType : true,
        IsSortingEnabled : true,
        // Pagination


        errormsg :  '',
        user_role: [],
        ListGrid : [],
        staffContactID:this.props.staffContactID,
        empHistoryStatusList:[],
        empHistoryJobTitleList:[],
        employeeTypeList:[],
        employeementHourList:[],
        employeementTimeTypeList:[],
        locationList:[],
        //Edit MOdel
        Editlocation :  '',
        EditstartDate :  '',
        EditendDate :  '',
        EditendemployeementHistoryId :  '',
        EditendpositionTitle :  '',
        EditcompanyName :  '',
        EditreasonForLeave :  '',
        EditjobDescription :  '',
        EditcompanyId :  '',
        EditstatusHis :  '',
        EditemployeeType :  '',
        EditemployeeHours :  '',
        EditpartTimeType :  '',
        EditjobTitle :  '',
        //Edit MOdel

        //Add MOdel
        Addlocation :  '',
        AddstartDate :  '',
        AddendDate :  '',
        AddendemployeementHistoryId :  '',
        AddendpositionTitle :  '',
        AddcompanyName :  '',
        AddreasonForLeave :  '',
        AddjobDescription :  '',
        AddcompanyId :  '',
        AddstatusHis :  '',
        AddemployeeType :  '',
        AddemployeeHours :  '',
        AddpartTimeType :  '',
        AddjobTitle :  '',
        //Add MOdel

        role_employment_history_can: {},

        isDelete : false,

        header_data : [],
        staffContactFullname : localStorage.getItem('fullName'),

        MaxEmployeeStartDate : ''
    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.handleAddstartDate = this.handleAddstartDate.bind(this);
    this.handleAddendDate = this.handleAddendDate.bind(this);

    this.handleEditstartDate = this.handleEditstartDate.bind(this);
    this.handleEditendDate = this.handleEditendDate.bind(this);
  }

  handleAddstartDate = (date) =>{
    // console.log('AddstartDate => '+ date);
    this.setState({ AddstartDate : date });
    this.setState({ AddendDate : '' });
  };

  handleAddendDate = (date) =>{
    // console.log('AddendDate => '+ date);
    this.setState({ AddendDate : date });
  };

  handleEditstartDate = (date) =>{
    // console.log('EditstartDate => '+ date);
    this.setState({ EditstartDate : date });
    this.setState({ EditendDate : '' });
  };

  handleEditendDate = (date) =>{
    // console.log('EditendDate => '+ date);
    this.setState({ EditendDate : date });
  };

  validationAddendDate = (currentDate) => {
    return currentDate.isAfter(moment(this.state.AddstartDate));
  };

  validationEditendDate = (currentDate) => {
    return currentDate.isAfter(moment(this.state.EditstartDate));
  };

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  handleChange = input => e => {
    this.setState({ [input]: e.target.value.replace(/^\s+/g, '') });
    // console.log(input);
    if (this.state[input] != '') {
        delete this.state.errormsg[input];
    }

    if([input]=="AddstartDate")
    {
      //this.setState({ AddendDate: moment(e.target.value).format('YYYY-MM-DD') });
    }

    if([input]=="EditstartDate")
    {
      //this.setState({ EditendDate: moment(e.target.value).format('YYYY-MM-DD') });
    }

    if([input]=="AddemployeeHours")
    {
      if(e.target.value == 'Full Time')
      {
        //Not Applicable
        this.setState({ AddpartTimeType: 'Not Applicable' });
        $('#AddpartTimeType').prop('disabled', true);
      }
      else
      {
        this.setState({ AddpartTimeType: '' });
        $('#AddpartTimeType').prop('disabled', false);
      }
      //AddpartTimeType
    }

    if([input]=="EditemployeeHours")
    {
      if(e.target.value == 'Full Time')
      {
        //Not Applicable
        this.setState({ EditpartTimeType: 'Not Applicable' });
        $('#EditpartTimeType').prop('disabled', true);
      }
      else
      {
        this.setState({ EditpartTimeType: '' });
        $('#EditpartTimeType').prop('disabled', false);
      }
    }
    
    
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  componentDidMount() {
    //this.setState({ Addlocation: this.props.primaryLocationId });
    this.setState({ Addlocation: this.props.primaryLocationId });
     // console.log("Addlocation");
     // console.log(this.props.primaryLocationId);
     
    
      /* Role Management */
       // console.log('Role Store employment_history_can');
       /*var getrole = SystemHelpers.GetRole();
       let employment_history_can = getrole.employment_history_can;
       this.setState({ role_employment_history_can: employment_history_can });
       // console.log(employment_history_can);*/

      // console.log(this.props.employment_history_can);
      let employment_history_can = this.props.employment_history_can;
      this.setState({ role_employment_history_can: this.props.employment_history_can });
      //console.log("did mo");
      //console.log(employment_history_can);
      /* Role Management */
    
    // this.GetUserEmployeementHistory();
    // this.GetProfile();

    // Delete Permison
    if(employment_history_can.employment_history_can_delete == true || employment_history_can.employment_history_can_delete == "true"){
      var columns = [
                /*{
                  label: 'Status',
                  field: 'status',
                  sort: 'asc',
                  width: 150
                },*/
                {
                  label: 'Job Title',
                  field: 'jobTitle',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Location',
                  field: 'location',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Employment Type',
                  field: 'employeeType',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Employment Hours',
                  field: 'employeeHours',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Part-time type',
                  field: 'partTimeType',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Start Date',
                  field: 'startDate',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'End Date',
                  field: 'endDate',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Record Status',
                  field: 'status',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'CreatedOn',
                  field: 'createdOn',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Emp. Status',
                  field: 'empStatus',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Action',
                  field: 'action',
                  width: 270
                }
              ];

      this.setState({ header_data: columns });
    }else{
      var columns = [
                /*{
                  label: 'Status',
                  field: 'status',
                  sort: 'asc',
                  width: 150
                },*/
                {
                  label: 'Job Title',
                  field: 'jobTitle',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Location',
                  field: 'location',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Employment Type',
                  field: 'employeeType',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Employment Hours',
                  field: 'employeeHours',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Part-time type',
                  field: 'partTimeType',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Start Date',
                  field: 'startDate',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'End Date',
                  field: 'endDate',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'CreatedOn',
                  field: 'createdOn',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Emp. Status',
                  field: 'empStatus',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Action',
                  field: 'action',
                  width: 270
                }
              ];

      this.setState({ header_data: columns });
    }
    // Delete Permison
  }

  TabClickOnLoadEmploymentHistory = () => e => {
    //debugger;
    e.preventDefault();

    //this.GetUserEmployeementHistory();
    this.setState({ sortColumn: 'StartDate' });
    this.setState({ SortType: true });

    this.setState({ currentPage: 1 });
    this.setState({ pageSize: 5 });
    this.setState({ searchText: '' });

    this.GetUserEmployeementHistory(1,5,'','StartDate',true);
    this.GetProfile();
  }

  GetProfile(){
    // console.log(localStorage.getItem("token"));
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserBasicInfoById?contactId='+this.state.staffContactID;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        // console.log("responseJson GetUserBasicInfoById PHSS Employment History ");
        // console.log(data);
        if (data.responseType === "1") {
            
            this.setState({ Addlocation: data.data.locationIdGuid });

            this.setState({ empHistoryStatusList: data.data.employeementStatusList });
            this.setState({ MaxEmployeeStartDate: data.data.maxEmployeeDate });
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                this.SessionOut();
              }else{
                this.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      //this.props.history.push("/error-500");
    });
  }

  TableHeaderDesign()
  {
      if(this.state.role_employment_history_can.employment_history_can_delete == true || this.state.role_employment_history_can.employment_history_can_delete == "true")
      {
        let columns = [];
        columns.push(<tr>
            <th>Job Title</th>
            <th>Location</th>
            <th>Employment Type</th>
            <th>Employment Hours</th>
            <th>Part-time type</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Record Status</th>
            <th>CreatedOn</th>
            <th>Emp. Status</th>
            <th>Action</th>
          </tr>
        );
        return columns;
      }
      else
      {
        let columns = [];
        columns.push(<tr>
            <th>Job Title</th>
            <th>Location</th>
            <th>Employment Type</th>
            <th>Employment Hours</th>
            <th>Part-time type</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>CreatedOn</th>
            <th>Emp. Status</th>
            <th>Action</th>
          </tr>
        );
        return columns; 
      }
  }

  GetUserEmployeementHistory(currentPage,pageSize,searchText,sortColumn,SortType){
    
    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    /* Role Management */

    this.showLoader();

    let canDelete = getrole.employment_history_can.employment_history_can_delete;


    this.setState({ ListGrid : [] });

    // Pagination
    let bodyarray = {};
    bodyarray["currentPage"] = 1;
    bodyarray["nextPage"] = false;
    bodyarray["pageSize"] = 5;
    bodyarray["previousPage"] = false;
    bodyarray["totalCount"] = 0;
    bodyarray["totalPages"] = 0;
    
    this.setState({ pagingData : bodyarray });

    this.setState({ currentPage: currentPage });
    this.setState({ pageSize: pageSize });

    /*var sort_Column = this.state.sortColumn;
    var Sort_Type = this.state.SortType;*/

    var sort_Column = sortColumn;
    var Sort_Type = SortType;
    
    var IsSortingEnabled = true;

    var url_paging_para = '&pageNumber='+currentPage+'&pageSize='+pageSize+'&searchText='+searchText+'&sortColumn='+sort_Column+'&SortType='+Sort_Type+'&IsSortingEnabled='+IsSortingEnabled;
    // Pagination

    var url=process.env.API_API_URL+'GetUserEmployeementHistory?contactId='+this.state.staffContactID+'&canDelete='+canDelete+url_paging_para;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUserEmployeementHistory");
        console.log(data);
        // console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            //this.setState({ ListGrid: data.data.userEmployeementHistoryViews });
            //this.setState({ ListGrid: this.rowData(data.data.userEmployeementHistoryViews) })
            this.setState({ header_data: this.TableHeaderDesign() });
            this.setState({ ListGrid: this.rowData(data.data.userEmployeementHistoryViews) });

            // this.setState({ empHistoryStatusList: data.data.empHistoryStatusList });
            this.setState({ empHistoryJobTitleList: data.data.empHistoryJobTitleList });
            this.setState({ employeeTypeList: data.data.employeeTypeList });
            this.setState({ employeementHourList: data.data.employeementHourList });
            this.setState({ employeementTimeTypeList: data.data.employeementTimeTypeList });
            this.setState({ locationList: data.data.employeementHistoryLocations });
            
            this.setState({ pagingData: data.pagingData });  
            
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                this.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('GetUserEmployeementHistory error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }

  
  Edit_Update_Btn_Func(emgContactId){
    let return_push = [];

    if(this.state.role_employment_history_can.employment_history_can_update == true || this.state.role_employment_history_can.employment_history_can_delete == true){
      let Edit_push = [];
      if(this.state.role_employment_history_can.employment_history_can_update == true){
        Edit_push.push(
          <a href="#" onClick={this.EditRecord(emgContactId)} className="dropdown-item" data-toggle="modal" data-target="#EmploymentInformation_PHSS_employment_history_Edit_modal"><i className="fa fa-pencil m-r-5" /> Edit</a>
        );
      }
      let Delete_push = [];
      if(this.state.role_employment_history_can.employment_history_can_delete == true){
        if(emgContactId.isDelete == false)
        {  
          Delete_push.push(
            <a href="#" onClick={this.EditRecord(emgContactId)} className="dropdown-item" data-toggle="modal" data-target="#delete_phss_employment_history"><i className="fa fa-trash-o m-r-5" /> Inactive</a>
          );
        }
        else
        {
          Delete_push.push(
            <a href="#" onClick={this.EditRecord(emgContactId)} className="dropdown-item" data-toggle="modal" data-target="#delete_phss_employment_history"><i className="fa fa-trash-o m-r-5" /> Active</a>
          );
        }  
      }
      
      return_push.push(
        <div className="dropdown dropdown-action">
          <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
          <div className="dropdown-menu dropdown-menu-right">
            {Edit_push}
            {Delete_push}
          </div>
        </div>
      );
    }
    return return_push;
  }

  EditRecord = (record) => e => {
    e.preventDefault();
    // console.log('Edit PHSS Employment History');
    // console.log(record);


    this.setState({ errormsg: '' });


    this.setState({ Editlocation: record.locationId });
    this.setState({ EditstartDate: moment(record.startDate,process.env.API_DATE_FORMAT) });
    this.setState({ EditendDate: moment(record.endDate,process.env.API_DATE_FORMAT) });
    // this.setState({ EditstartDate: moment(record.startDate).format('YYYY-MM-DD') });
    // this.setState({ EditendDate: moment(record.endDate).format('YYYY-MM-DD') });
    this.setState({ EditendemployeementHistoryId: record.employeementHistoryId });
    this.setState({ EditendpositionTitle: record.positionTitle });
    this.setState({ EditcompanyName: record.companyName });
    this.setState({ EditreasonForLeave: record.reasonForLeave });
    this.setState({ EditjobDescription: record.jobDescription });
    this.setState({ EditcompanyId: record.companyId });

    //this.setState({ Editstatus: record.status });
    
    
    this.setState({ EditemployeeType: record.employeeType });
    this.setState({ EditemployeeHours: record.employeeHours });
    this.setState({ EditpartTimeType: record.partTimeType });
    this.setState({ EditjobTitle: record.jobTitleId });
    this.setState({ EditstatusHis: record.status });

    if(record.employeeHours == 'Full Time')
    {
      //Not Applicable
      this.setState({ EditpartTimeType: 'Not Applicable' });
      $('#EditpartTimeType').prop('disabled', true);
    }
    else
    {
      //this.setState({ EditpartTimeType: '' });
      this.setState({ EditpartTimeType: record.partTimeType });
      $('#EditpartTimeType').prop('disabled', false);
    }

    this.setState({ isDelete: record.isDelete });

  }

  UpdateRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};
    

    var EditjobTitle = $('#EditjobTitle').val();
    // console.log(EditjobTitle);
    if (this.state["EditjobTitle"] == '') {
      step1Errors["EditjobTitle"] = "Job Title is mandatory";
    }

    var EditstatusHis = $('#EditstatusHis').val()
    if (this.state["EditstatusHis"] == '') {
      step1Errors["EditstatusHis"] = "Status is mandatory";
    }

    if (this.state["Editlocation"] == '') {
      step1Errors["Editlocation"] = "Location is mandatory";
    }

    var EditemployeeType = $('#EditemployeeType').val()
    if (this.state["EditemployeeType"] == '') {
      step1Errors["EditemployeeType"] = "Employment Type is mandatory";
    }

    var EditemployeeHours = $('#EditemployeeHours').val()
    if (this.state["EditemployeeHours"] == '') {
      step1Errors["EditemployeeHours"] = "Employment Hours is mandatory";
    }

    
    if (this.state["EditpartTimeType"] == '') {
      step1Errors["EditpartTimeType"] = "Part-time type is mandatory";
    }

    if (this.state["EditstartDate"] == '') {
      step1Errors["EditstartDate"] = "Start Date is mandatory";
    }

    // if (this.state["EditendDate"] == '') {
    //   step1Errors["EditendDate"] = "End Date is mandatory";
    // }

    //var dateOfBirth="";
    if(this.state["EditendDate"] !=="" && this.state["EditendDate"] !== null && this.state["EditendDate"] !== "NaN"){
      if(this.state["EditendDate"].length){
        SystemHelpers.ToastWarning('please select a valid date.');
        return false;
      } 
    }

    

    // console.log(step1Errors);

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }

    //return false;

    this.showLoader();
    var EditstartDate=moment(this.state["EditstartDate"]).format('MM-DD-YYYY');
    //var NewEditstartDate = moment(EditstartDate, "MM-DD-YYYY").add(1, 'days');

    var NewEditendDate ='';
    if (this.state["EditendDate"] != '') {
      var NewEditendDate=moment(this.state["EditendDate"]).format('MM-DD-YYYY');
      //var NewEditendDate = moment(EditendDate, "MM-DD-YYYY").add(1, 'days');
    }
    
    let ArrayJson = {

          //jobTitle: this.state["EditjobTitle"],
          JobTitleId : this.state["EditjobTitle"],
          locationId: this.state["Editlocation"],
          employeeType: this.state["EditemployeeType"],
          employeeHours: this.state["EditemployeeHours"],
          partTimeType: this.state["EditpartTimeType"],
          startDate: EditstartDate,
          endDate: NewEditendDate,
          status: this.state["EditstatusHis"],
          employeementHistoryId: this.state["EditendemployeementHistoryId"]
    };
     
    let bodyarray = {};
    bodyarray["contactId"] = this.state.staffContactID;
    bodyarray["userEmployeementHistory"] = ArrayJson;
    bodyarray["userName"] = this.state.staffContactFullname;

    // console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'UpdateEmployeementHistory';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        // console.log("responseJson UpdateEmployeementHistory");
        // console.log(data);
        // console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');
            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".close" ).trigger( "click" );
            this.GetUserEmployeementHistory(this.state.currentPage,this.state.pageSize,this.state.searchText,this.state.sortColumn,this.state.SortType);
        }
        else{
            SystemHelpers.ToastError(data.message  );
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('UpdateEmployeementHistory error');
      console.log(error);
      this.props.history.push("/error-500");
    });
    return false;
  }

  ClearRecord = ()=> e => {
    
    e.preventDefault();

    this.setState({ AddjobTitle: 0 });
    this.setState({ AddstatusHis: 0 });
    //this.setState({ Addlocation: 0 });
    this.setState({ AddemployeeType: 0 });
    this.setState({ AddemployeeHours: 0 });
    this.setState({ AddpartTimeType: 0 });
    this.setState({ AddstartDate: '' });
    this.setState({ AddendDate: '' });
    this.setState({ Addlocation: localStorage.getItem("primaryLocationGuid") });
    //this.GetProfile();
    
    $('#AddpartTimeType').prop('disabled', false);

    this.setState({ errormsg: '' });
  }

  AddRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};
    
    if (this.state["AddjobTitle"] =='') {
      step1Errors["AddjobTitle"] = "Job Title is mandatory";
    }

    if (this.state["AddstatusHis"] == '') {
      step1Errors["AddstatusHis"] = "Status is mandatory";
    }

    if (this.state["Addlocation"] == '') {
      step1Errors["Addlocation"] = "Location is mandatory";
    }

   
    if (this.state["AddemployeeType"] == '') {
      step1Errors["AddemployeeType"] = "Employment Type is mandatory";
    }

    if (this.state["AddemployeeHours"]  == '') {
      step1Errors["AddemployeeHours"] = "Employment Hours is mandatory";
    }

    if (this.state["AddpartTimeType"] == '') {
      step1Errors["AddpartTimeType"] = "Part-time type is mandatory";
    }

    if (this.state["AddstartDate"] == '') {
      step1Errors["AddstartDate"] = "Start Date is mandatory";
    }

    // if (this.state["AddendDate"] == '') {
    //   step1Errors["AddendDate"] = "End Date is mandatory";
    // }

    if(this.state["AddendDate"] !=="" && this.state["AddendDate"] !== null && this.state["AddendDate"] !== "NaN"){
      if(this.state["AddendDate"].length){
        SystemHelpers.ToastWarning('please select a valid date.');
        return false;
      } 
    }

    

    // console.log(step1Errors);

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }

    //return false;

    this.showLoader();
    var AddstartDate=moment(this.state["AddstartDate"]).format('MM-DD-YYYY');
    //var NewAddstartDate = moment(AddstartDate, "MM-DD-YYYY").add(1, 'days');
    
    var add_enddate ='';
    if (this.state["AddendDate"] != '') {
      var add_enddate=moment(this.state["AddendDate"]).format('MM-DD-YYYY');
      //var NewAddendDate = moment(AddendDate, "MM-DD-YYYY").add(1, 'days');
      //var add_enddate=moment(NewAddendDate).format('MM-DD-YYYY');
    }

    let ArrayJson = {

          //jobTitle: this.state["AddjobTitle"],
          JobTitleId: this.state["AddjobTitle"],
          locationId: this.state["Addlocation"],
          //location: '',
          employeeType: this.state["AddemployeeType"],
          employeeHours: this.state["AddemployeeHours"],
          partTimeType: this.state["AddpartTimeType"],
          //startDate: moment(NewAddstartDate).format('MM-DD-YYYY'),
          startDate: AddstartDate,
          endDate:add_enddate ,
          status: this.state["AddstatusHis"]
    };
     
    let bodyarray = {};
    bodyarray["contactId"] = this.state.staffContactID;
    bodyarray["userEmployeementHistory"] = ArrayJson;

    // console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'CreateEmployeementHistory';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        // console.log("responseJson CreateEmployeementHistory");
        // console.log(data);
        // console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');

            this.setState({ AddjobTitle: 0 });
            this.setState({ AddstatusHis: 0 });
            this.setState({ Addlocation: 0 });
            this.setState({ AddemployeeType: 0 });
            this.setState({ AddemployeeHours: 0 });
            this.setState({ AddpartTimeType: 0 });
            this.setState({ AddstartDate: '' });
            this.setState({ AddendDate: '' });
            
            $('#AddpartTimeType').prop('disabled', false);

            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".close" ).trigger( "click" );
            this.GetUserEmployeementHistory(this.state.currentPage,this.state.pageSize,this.state.searchText,this.state.sortColumn,this.state.SortType);
               
        }
        else{
            SystemHelpers.ToastError(data.message  );
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('CreateEmployeementHistory error');
      console.log(error);
      this.props.history.push("/error-500");
    });
    return false;
  }

  DeleteRecord = () => e => {
    e.preventDefault();

    var isdelete = '';
    if(this.state.isDelete== true)
    {
      isdelete = false;
    }
    else
    {
      isdelete = true;
    }

    this.showLoader();

    // console.log(this.state.EditendemployeementHistoryId);

    var url=process.env.API_API_URL+'DeleteUserEmployementHistory?employeementHistoryId='+this.state.EditendemployeementHistoryId+'&isDelete='+isdelete+'&userName='+this.state.staffContactFullname;
    fetch(url, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        // console.log("responseJson DeleteUserEmployementHistory");
        // console.log(data);
        // console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.GetUserEmployeementHistory(this.state.currentPage,this.state.pageSize,this.state.searchText,this.state.sortColumn,this.state.SortType);
            this.hideLoader();
        }else if (data.responseType == "2" || data.responseType == "3") {
            SystemHelpers.ToastError(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.hideLoader();
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                this.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              this.hideLoader();
              $( ".cancel-btn" ).trigger( "click" );
        }
        
        
    })
    .catch(error => {
      console.log('DeleteUserEmployementHistory error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }

  rowData(ListGrid) {
    // console.log("row emp history ListGrid")
    // console.log(ListGrid)
    /*console.log("**********************************************");
    console.log(this.state.role_employment_history_can);
    console.log("**********************************************");*/
    /* Role Management */
     //var getrole = SystemHelpers.GetRole();
    // let canDelete = getrole.employment_history_can.employment_history_can_delete;
    // console.log("rowdata Permison =");
    // console.log(getrole.employment_history_can);
    // console.log(this.state.role_employment_history_can);
    // console.log(getrole);
    let canDelete = this.state.role_employment_history_can.employment_history_can_delete;
    /* Role Management */

    

      var ListGrid_length = ListGrid.length;
      let dataArray = [];
      var i=1;
      for (var z = 0; z < ListGrid_length; z++) {
        var tempdataArray = [];
        //tempdataArray.rownum = i;
        //tempdataArray.status = ListGrid[z].status;

        var end_date ="";
        if(ListGrid[z].endDate != "" && ListGrid[z].endDate != null){
          end_date = moment(ListGrid[z].endDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT);
        }

        var is_detele = "";
        if(canDelete == true){
          
          if(ListGrid[z].isDelete == true){
            is_detele = <div><span class="badge bg-inverse-warning">Inactive</span></div>;
          }else{
            is_detele = <div><span class="badge bg-inverse-success">Active</span></div>;
          }
        }

        var create_on =  SystemHelpers.TimeZone_DateTime(ListGrid[z].createdOn);

        var status_emp = "";
        if(ListGrid[z].empStatus == "Active"){
          status_emp = <div><span class="badge bg-inverse-success">Active</span></div>;
        }else if(ListGrid[z].empStatus == "On Hold"){
          status_emp = <div><span class="badge bg-inverse-warning">On Hold</span></div>;
        }else if(ListGrid[z].empStatus == "Inactive"){
          status_emp = <div><span class="badge bg-inverse-danger">Inactive</span></div>;  
        }  

        tempdataArray.push(<tr key={z}>
            <td>{ListGrid[z].jobTitle}</td>
            <td>{ListGrid[z].location}</td>
            <td>{ListGrid[z].employeeType}</td>
            <td>{ListGrid[z].employeeHours}</td>
            <td>{ListGrid[z].partTimeType}</td>
            <td>{moment(ListGrid[z].startDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</td>
            <td>{end_date}</td>
            <td>{is_detele}</td>
            <td>{create_on}</td>
            <td>{status_emp}</td>  
            <td>{this.Edit_Update_Btn_Func(ListGrid[z])}</td>  
          </tr>);
          dataArray.push(tempdataArray);
        i++;
      }

      return dataArray;
  }


  // Pagination Design
  PaginationDesign ()
  {
    let PageOutput = [];
    // console.log('pagination');
    // console.log(this.state.pagingData);
    
    if(this.state.pagingData !="" && this.state.pagingData !="undefined")
    {
      var Page_Count = this.state.pagingData.totalPages;
      //alert(this.state.pagingData.currentPage);
    // console.log('page count = ' + Page_Count);
    /* pagination count */


        var Page_Start=1;
        var Page_End=1;

        if(this.state.pagingData.currentPage == 1){
            Page_Start=1;

            if(Page_Count <= 10){
                Page_End=Page_Count;
            }else{
                Page_End=10;
            }
            
        }else{

            if(this.state.pagingData.currentPage < 5){
                Page_Start=1;
                Page_End=Page_Count;
                //Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
                // console.log("Page_End 1 "+ Page_End);
            }else{
                Page_Start=parseInt(this.state.pagingData.currentPage) - parseInt(4);
                Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
                // console.log("Page_End 2 "+ Page_End);
                if(Page_End > Page_Count){
                    Page_End=Page_Count;
                    // console.log("Page_End 3 "+ Page_End);
                }
            }

        }
      let Page = [];
      var i = 1;
      for (var z=Page_Start; z <= Page_End ; z++)
      {
        if(z==this.state.pagingData.currentPage)
        {
          Page.push(<li className="page-item active pk-active">
            <a className="page-link pk-active" id={z} href="#" onClick={this.PageGetGridData}>{z}<span className="sr-only">(current)</span></a>
          </li>);
        }
        else
        {
          Page.push(<li className="page-item"><a className="page-link" id={z} href="#" onClick={this.PageGetGridData} >{z}</a></li>);
        }
        i++;
      }

      let PagePrev = [];

      if(this.state.pagingData.currentPage == 1){
        PagePrev.push(<li className="page-item disabled">
          <a className="page-link" href="#">Previous</a>
        </li>);
      }else{
        PagePrev.push(<li className="page-item">
          <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)-parseInt(1)} tabIndex={-1} onClick={this.PageGetGridData}>Previous</a>
        </li>);
      }

      let PageNext = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageNext.push(<li className="page-item disabled">
          <a className="page-link" href="#">Next</a>
        </li>);
      }else{
        PageNext.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)+parseInt(1)} onClick={this.PageGetGridData}>Next</a>
          </li>
        );
      }

      let PageLast = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageLast.push(<li className="page-item disabled">
          <a className="page-link" href="#">Last</a>
        </li>);
      }else{
        PageLast.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(Page_Count)} onClick={this.PageGetGridData}>Last</a>
          </li>
        );
      }



      PageOutput.push(<section className="comp-section" id="comp_pagination">
                        <div className="pagination-box">
                          <div>
                            <ul className="pagination">
                              
                              {PagePrev}
                              {Page}
                              {PageNext}
                              {PageLast}
                              
                            </ul>
                          </div>
                        </div>
                      </section>);
    }
    
    return PageOutput;
  }

  PageGetGridData = e => {

    e.preventDefault();
    let current_page= e.target.id;
    this.GetUserEmployeementHistory(current_page,this.state.pageSize,this.state.searchText,this.state.sortColumn,this.state.SortType)
  }

  SearchGridData = e => {
    this.setState({ pageSize: this.state.TempsearchText });
    this.GetUserEmployeementHistory(1,this.state.pageSize,this.state.TempsearchText,this.state.sortColumn,this.state.SortType);
  }
  // Pagination Design

  render() {
      const data = {
        columns: this.state.header_data,
        rows: this.state.ListGrid
      };
     
      return (
        <div>
        {/* Toast & Loder method use */}
            
        {(this.state.loading) ? <Loader /> : null} 
        {/* Toast & Loder method use */}
        <div className="row">
          <div className="col-md-12 d-flex">
            <div className="card profile-box flex-fill">
              <div className="row">
                <button className="btn btn-primary submit-btn pk-profiletab-refreshbtn-hide" id="TabClickOnLoadEmploymentHistory" onClick={this.TabClickOnLoadEmploymentHistory()}>Refresh</button>
              </div>
              <div className="card-body">
                {this.state.role_employment_history_can.employment_history_can_create == true ?
                  <h3 className="card-title">PHSS Employment History<a href="#" className="edit-icon" data-toggle="modal" data-target="#EmploymentInformation_PHSS_employment_history_add_modal"><i className="fa fa-plus" /></a></h3>
                  : <h3 className="card-title">PHSS Employment History <a href="#" className="phss-lock"><i className="fa fa-lock" /></a></h3>
                }
                
                {/* Page Per Record and serach design*/}
                <div className="row filter-row">
                  <div className="col-sm-6 col-md-2"> 
                    <div className="form-group form-focus select-focus">
                      <select className="form-control floating" value={this.state.pageSize}  onChange={this.handleChange('pageSize')}> 
                        <option value="5">5/Page</option>
                        <option value="10">10/Page</option>
                        <option value="50">50/Page</option>
                        <option value="100">100/Page</option>
                      </select>
                      <label className="focus-label">Per Page</label>
                    </div>
                  </div>
                  
                  <div className="col-sm-6 col-md-3">
                    <div className="form-group form-focus focused">
                      <label className="focus-label">Sorting</label>
                      <select className="form-control floating" id="sortColumn" value={this.state.sortColumn} onChange={this.handleChange('sortColumn')}> 
                        <option value="">-</option>
                        <option value="JobTitle">Job Title</option>
                        <option value="Location">Location</option>
                        <option value="EmployeeType">Employment Type</option>
                        <option value="EmployeeHours">Employment Hours</option>
                        <option value="StartDate">Start Date</option>
                        <option value="EndDate">End Date</option>
                      </select>
                    </div>
                  </div> 

                  <div className="col-sm-6 col-md-2">
                    <div className="form-group form-focus focused">
                      <label className="focus-label">Sorting Order</label>
                      <select className="form-control floating" id="SortTypeId" value={this.state.SortType} onChange={this.handleChange('SortType')}> 
                        {/*<option value="">-</option>*/}
                        <option value="false">Ascending</option>
                        <option value="true">Descending</option>
                      </select>
                    </div>
                  </div> 

                  <div className="col-sm-6 col-md-3">  
                    <div className="form-group form-focus focused">
                      <input className="form-control floating" type="text" value={this.state.TempsearchText}  onChange={this.handleChange('TempsearchText')} placeholder="Search by Location" />
                      <label className="focus-label">Search</label>
                    </div>
                  </div>

                  <div className="col-sm-6 col-md-2">  
                    <a href="#" className="btn btn-success btn-block" onClick={this.SearchGridData}> Search </a>  
                  </div> 
                </div>
                {/* Page Per Record and serach design*/}

                <div className="row">
                  <div className="col-md-12">
                      <div className="table-responsive">
                      
               
                        
                          
                        <table className="table table-striped custom-table mb-0 datatable">
                          <thead>
                            { this.state.ListGrid.length > 0 ? this.state.header_data : null}
                          </thead>
                          <tbody>
                            
                            {this.state.ListGrid.length > 0 ? this.state.ListGrid : null}
                          </tbody>
                        </table> 

                      {this.PaginationDesign()}

                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
        {/* ********** Employment Information Tab Modals *********** */}
            {/* PHSS Employment History Modal */}
            <div id="EmploymentInformation_PHSS_employment_history_add_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">PHSS Employment History</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()} >
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Job Title<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.AddjobTitle} onChange={this.handleChange('AddjobTitle')}>
                                  <option value="">-</option>
                                  {this.state.empHistoryJobTitleList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.jobTitleId}>{listValue.jobTitleName}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddjobTitle"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Status<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.AddstatusHis} onChange={this.handleChange('AddstatusHis')}>
                                  <option value="">-</option>
                                  {this.state.empHistoryStatusList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddstatusHis"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                              
                                <label>Location<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.Addlocation} onChange={this.handleChange('Addlocation')}>
                                  <option value="">-</option>
                                  {this.state.locationList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.id}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["Addlocation"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Employment Type<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.AddemployeeType} onChange={this.handleChange('AddemployeeType')}>
                                  <option value="">-</option>
                                  {this.state.employeeTypeList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddemployeeType"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Employment Hours<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.AddemployeeHours} onChange={this.handleChange('AddemployeeHours')}>
                                  <option value=''>-</option>
                                  {this.state.employeementHourList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddemployeeHours"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Part-time type<span className="text-danger">*</span></label>
                                <select className="form-control" id="AddpartTimeType" value={this.state.AddpartTimeType} onChange={this.handleChange('AddpartTimeType')}>
                                  <option value=''>-</option>
                                  {this.state.employeementTimeTypeList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                  
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddpartTimeType"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Start Date<span className="text-danger">*</span></label>
                                <Datetime
                                  inputProps={{readOnly: true}}
                                  closeOnTab={true}
                                  input={true}
                                  value={(this.state.AddstartDate) ? this.state.AddstartDate : ''}
                                  onChange={this.handleAddstartDate}
                                  dateFormat={process.env.DATE_FORMAT}
                                  timeFormat={false}
                                  renderInput={(props) => {
                                     return <input {...props} value={(this.state.AddstartDate) ? props.value : ''} />
                                  }}
                                />
                                {/*<input className="form-control" type="date" value={this.state.AddstartDate} onChange={this.handleChange('AddstartDate')}/>*/}
                                <span className="form-text error-font-color">{this.state.errormsg["AddstartDate"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>End Date</label>
                                <Datetime
                                isValidDate={this.validationAddendDate}
                                inputProps={{readOnly: false}}
                                closeOnTab={true}
                                input={true}
                                value={(this.state.AddendDate) ? this.state.AddendDate : ''}
                                onChange={this.handleAddendDate}
                                dateFormat={process.env.DATE_FORMAT}
                                timeFormat={false}
                                renderInput={(props) => {
                                   return <input {...props} value={(this.state.AddendDate) ? props.value : ''} />
                                }}
                              />
                                {/*<input className="form-control" type="date" value={this.state.AddendDate} onChange={this.handleChange('AddendDate')} min={moment().format(this.state.AddstartDate,"YYYY-MM-DD")} />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["AddendDate"]}</span>
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" onClick={this.AddRecord()}>Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* //PHSS Employment History Add Modal */}

            {/* PHSS Employment History Edit Modal */}
            <div id="EmploymentInformation_PHSS_employment_history_Edit_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">PHSS Employment History</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Job Title<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.EditjobTitle} onChange={this.handleChange('EditjobTitle')}>
                                  <option value="">-</option>
                                  {this.state.empHistoryJobTitleList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.jobTitleId}>{listValue.jobTitleName}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["EditjobTitle"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Status<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.EditstatusHis} onChange={this.handleChange('EditstatusHis')}>
                                  <option value="">-</option>
                                  {this.state.empHistoryStatusList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                  
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["EditstatusHis"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Location<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.Editlocation} onChange={this.handleChange('Editlocation')}>
                                  <option value="">-</option>
                                  {this.state.locationList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.id}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["Editlocation"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Employment Type<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.EditemployeeType} onChange={this.handleChange('EditemployeeType')}>
                                  <option value="">-</option>
                                  {this.state.employeeTypeList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["EditemployeeType"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Employment Hours<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.EditemployeeHours} onChange={this.handleChange('EditemployeeHours')}>
                                  <option value=''>-</option>
                                  {this.state.employeementHourList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["EditemployeeHours"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Part-time type<span className="text-danger">*</span></label>
                                <select className="form-control" id="EditpartTimeType" value={this.state.EditpartTimeType} onChange={this.handleChange('EditpartTimeType')}>
                                  <option value=''>-</option>
                                  {this.state.employeementTimeTypeList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["EditpartTimeType"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Start Date<span className="text-danger">*</span></label>
                                <Datetime
                                  inputProps={{readOnly: true}}
                                  closeOnTab={true}
                                  input={true}
                                  value={(this.state.EditstartDate) ? this.state.EditstartDate : ''}
                                  onChange={this.handleEditstartDate}
                                  dateFormat={process.env.DATE_FORMAT}
                                  timeFormat={false}
                                  renderInput={(props) => {
                                     return <input {...props} value={(this.state.EditstartDate) ? props.value : ''} />
                                  }}
                                />
                                {/*<input className="form-control" type="date" value={this.state.EditstartDate} onChange={this.handleChange('EditstartDate')}/>*/}
                                <span className="form-text error-font-color">{this.state.errormsg["EditstartDate"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>End Date</label>
                                <Datetime
                                  inputProps={{readOnly: false}}
                                  isValidDate={this.validationEditendDate}
                                  closeOnTab={true}
                                  input={true}
                                  value={(this.state.EditendDate) ? this.state.EditendDate : ''}
                                  onChange={this.handleEditendDate}
                                  dateFormat={process.env.DATE_FORMAT}
                                  timeFormat={false}
                                  renderInput={(props) => {
                                     return <input {...props} value={(this.state.EditendDate) ? props.value : ''} />
                                  }}
                                />
                                {/*<input className="form-control" type="date" value={this.state.EditendDate} onChange={this.handleChange('EditendDate')} min={moment().format(this.state.EditstartDate,"YYYY-MM-DD")} />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["EditendDate"]}</span>
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" onClick={this.UpdateRecord()}>Update</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* //PHSS Employment History Edit Modal */}
            {/* Delete Trained Locations  Modal */}
            <div className="modal custom-modal fade" id="delete_phss_employment_history" role="dialog">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-body">
                    <div className="form-header">
                      <h3>PHSS Employment History</h3>
                      <p>Are you sure you want to mark employment history as {this.state.isDelete == true ? 'Active' : 'Inactive' } ?</p>
                    </div>
                    <div className="modal-btn delete-action">
                      <div className="row">
                        <div className="col-6">
                          <a  onClick={this.DeleteRecord()} className="btn btn-primary continue-btn">{this.state.isDelete == true ? 'Active' : 'Inactive' }</a>
                        </div>
                        <div className="col-6">
                          <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">Cancel</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        {/* /Delete Trained Locations Modal */}
        {/* ********** Employment Information Tab Modals *********** */}
        </div>
      );
   }
}

export default EmploymentHistory;
