/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";


import moment from 'moment';
import Loader from '../../../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {Avatar_02,Avatar_05,Avatar_09,Avatar_10,Avatar_16 } from '../../../../Entryfile/imagepath'
import FileUploadPreview from '../../../FileUpload/FileUploadPreview'
import UpdateFileUploadPreview from '../../../FileUpload/UpdateFileUploadPreview'
import DocxImg from '../../../../assets/img/doc/docx.png'
import ExcelImg from '../../../../assets/img/doc/excel.png'
import PdfImg from '../../../../assets/img/doc/pdf.png'

import FileHelpers from '../../../Helpers/FileUploadHelper';
import SystemHelpers from '../../../Helpers/SystemHelper';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';
//table

import Datetime from "react-datetime";

class ConsentsAndWaivers extends Component {
  constructor(props) {
    super(props);

    this.state = {

        // Pagination 
        totalCount : 0,
        pageSize : 5,
        currentPage : 1,
        totalPages : 0,
        previousPage : false,
        nextPage : false,
        searchText : '',
        pagingData : {},
        TempsearchText:'',

        sortColumn : 'IssueDate',
        SortType : true,
        IsSortingEnabled : true,
        // Pagination

        errormsg :  '',
        user_role: [],
        ListGrid : [],
        staffContactID:this.props.staffContactID,
        // Edit Model
        EditconsentId :  '',
        Editname :  '',
        EditissueDate :  '',
        EditexpiryDate :  '',
        Editdescription :  '',
        Editstatus :  '',
        Editnote :  '',
        Editattachement :  '',
        UpdatefilePreviewsFinal:[],
        TempEditattachement:'',
        EditconsentType : '',
        devfiles: [],
        devevents: {},
        UpdatefilePreviews: [],
        //UpdatefilePreviewsFinal: [],
        // Edit Model

        // Add Model
        
        Addname :  '',
        AddissueDate :  '',
        AddexpiryDate :  '',
        Adddescription :  '',
        Addstatus :  'na',
        Addnote :  '',
        Addattachement :  '',
        filePreviewsFinal:[],
        AddconsentType : '',
        // Add Model

        ConsentType : [],
        AddResetflag:false,
        EditResetflag: false,
        role_consents_waivers_can:{},

        isDelete : false,

        header_data : [],

        filePreviewsFinalEdit:[],
        filePreviewsFinalConsentsEditActive:0,
        staffContactFullname : localStorage.getItem('fullName')
    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.handleAddissueDate = this.handleAddissueDate.bind(this);
    this.handleAddexpiryDate = this.handleAddexpiryDate.bind(this);

    this.handleEditissueDate = this.handleEditissueDate.bind(this);
    this.handleEditexpiryDate = this.handleEditexpiryDate.bind(this);
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  handleAddissueDate = (date) =>{
    // console.log('AddissueDate => '+ date);
    this.setState({ AddissueDate : date });
    this.setState({ AddexpiryDate : '' });
  };

  handleAddexpiryDate = (date) =>{
    // console.log('AddexpiryDate => '+ date);

    if(this.state.AddissueDate != ''){

        var checkdate= moment(date).isAfter(moment());
      //alert(checkdate);
      if(checkdate == false){
        this.setState({ ["Addstatus"]: false });
        
      }else{
        this.setState({ ["Addstatus"]: true });
      }

      var TodayDate= moment(new Date()).format('YYYY-MM-DD');
      var CheckExpiryDate= moment(date).format('YYYY-MM-DD');

      if(TodayDate == CheckExpiryDate){
        this.setState({ ["Addstatus"]: true });
      }
      
      
    }

    this.setState({ AddexpiryDate : date });
  };

  handleEditissueDate = (date) =>{
    // console.log('EditissueDate => '+ date);
    this.setState({ EditissueDate : date });


    this.setState({ EditexpiryDate : '' });
  };

  handleEditexpiryDate = (date) =>{
    // console.log('EditexpiryDate => '+ date);


    if(this.state.EditissueDate != ''){

      var checkdate= moment(date).isAfter(moment());
      //alert(checkdate);
      if(checkdate == false){
        this.setState({ ["Editstatus"]: false });
        
      }else{
        this.setState({ ["Editstatus"]: true });
      }

      var TodayDate= moment(new Date()).format('YYYY-MM-DD');
      var CheckExpiryDate= moment(date).format('YYYY-MM-DD');

      if(TodayDate == CheckExpiryDate){
        this.setState({ ["Editstatus"]: true });
      }
      delete this.state.errormsg["Editstatus"];
      
    }
    
    this.setState({ EditexpiryDate : date });
  };

  validationAddexpiryDate = (currentDate) => {
    return currentDate.isAfter(moment(this.state.AddissueDate));
  };

  validationEditexpiryDate = (currentDate) => {
    return currentDate.isAfter(moment(this.state.EditissueDate));
  };

  handleChange = input => e => {
    this.setState({ [input]: e.target.value.replace(/^\s+/g, '') });
    // console.log(input);
    if (this.state[input] != '') {
        delete this.state.errormsg[input];
    }

    if([input]=="AddissueDate")
    {
      //this.setState({ AddexpiryDate: moment(e.target.value).format('YYYY-MM-DD') });
      this.setState({ AddexpiryDate: '' });
    }

    if([input]=="AddexpiryDate")
    {
      if(this.state.AddissueDate != ''){

        var checkdate= moment(e.target.value).isAfter(moment());
        //alert(checkdate);
        if(checkdate == false){
          this.setState({ ["Addstatus"]: false });
          
        }else{
          this.setState({ ["Addstatus"]: true });
        }

        var TodayDate= moment(new Date()).format('YYYY-MM-DD');
        var CheckExpiryDate= moment(e.target.value).format('YYYY-MM-DD');

        if(TodayDate == CheckExpiryDate){
          this.setState({ ["Addstatus"]: true });
        }
        delete this.state.errormsg["Addstatus"];
        
      }
    }

    if([input]=="EditexpiryDate")
    {
      if(this.state.EditissueDate != ''){

        var checkdate= moment(e.target.value).isAfter(moment());
        //alert(checkdate);
        if(checkdate == false){
          this.setState({ ["Editstatus"]: false });
          
        }else{
          this.setState({ ["Editstatus"]: true });
        }

        var TodayDate= moment(new Date()).format('YYYY-MM-DD');
        var CheckExpiryDate= moment(e.target.value).format('YYYY-MM-DD');

        if(TodayDate == CheckExpiryDate){
          this.setState({ ["Editstatus"]: true });
        }
        delete this.state.errormsg["Editstatus"];
        
      }
    }

    
    
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  componentDidMount() {
     // console.log("consents_waivers");
     
      
      /* Role Management */
      // console.log('Role Store consents_waivers_can');
      /*var getrole = SystemHelpers.GetRole();
      let consents_waivers_can = getrole.consents_waivers_can;
      this.setState({ role_consents_waivers_can: consents_waivers_can });
      // console.log(consents_waivers_can);*/

      // console.log(this.props.consents_waivers_can);
      let consents_waivers_can = this.props.consents_waivers_can;
      this.setState({ role_consents_waivers_can: this.props.consents_waivers_can });
      /* Role Management */

      //this.GetUserConsentsWaivers();

      // Delete Permison
      // Delete Permison
  }

  TabClickOnLoadConsentsWaivers = () => e => {
    //debugger;
    e.preventDefault();

    this.GetUserConsentsWaivers(this.state.currentPage,this.state.pageSize,this.state.searchText);
  }

  CreateBaseUrl(extension,src){
      var temp_url='';
      if(extension == 'image/png'){
        temp_url='data:image/png;base64,'+src;
      }

      if(extension == 'image/jpeg'){
        temp_url='data:image/jpg;base64,'+src;
      }

      if(extension == 'image/jpg'){
        temp_url='data:image/jpg;base64,'+src;
      }

      if(extension == 'application/vnd.ms-excel'){
        temp_url='data:application/vnd.ms-excel;base64,'+src;
      }

      if(extension == 'application/pdf'){
        temp_url='data:application/pdf;base64,'+src;
      }

      if(extension == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
        temp_url='data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,'+src;
      }

      if(extension == 'application/vnd.ms-excel'){
        temp_url='data:application/vnd.ms-excel;base64,'+src;
      }

      if(extension == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'){
        temp_url='data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,'+src;
      }

      if(extension == 'application/msword'){
        temp_url='data:application/msword;base64,'+src;
      }

      // console.log(temp_url)
      return temp_url;
  }

  ImageReturn(base64String,fileType){
    if(base64String !=''){
      var imgsrc  ='';
      if(fileType == 'image/png'){
        imgsrc=this.CreateBaseUrl(fileType,base64String);
      }else if(fileType == 'image/jpeg'){
          imgsrc=this.CreateBaseUrl(fileType,base64String);
      }else if(fileType == 'image/jpg'){
          imgsrc=this.CreateBaseUrl(fileType,base64String);
      }else if(fileType == 'application/vnd.ms-excel'){
          imgsrc=ExcelImg;
      }else if(fileType == 'application/pdf'){
          imgsrc=PdfImg;
      }else if(fileType == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
          imgsrc=ExcelImg;
      }else if(fileType == 'application/vnd.ms-excel'){
          imgsrc=ExcelImg;
      }else if(fileType == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'){
          imgsrc=DocxImg;
      }

    
      return <img className="grid-img" src={imgsrc} alt="" height="15" />;
    }else{
      return "";
    }
  }

  TableHeaderDesign()
  {
      if(this.state.role_consents_waivers_can.consents_waivers_can_delete == true || this.state.role_consents_waivers_can.consents_waivers_can_delete == "true")
      {
        let columns = [];
        columns.push(<tr>
            <th>Name</th>
            <th>Issue Date</th>
            <th>Expiry Date</th>
            <th>Expiry Status</th>
            <th>Consent Type</th>
            <th>Attachment</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        );
        return columns;
      }
      else
      {
        let columns = [];
        columns.push(<tr>
            <th>Name</th>
            <th>Issue Date</th>
            <th>Expiry Date</th>
            <th>Expiry Status</th>
            <th>Consent Type</th>
            <th>Attachment</th>
            <th>Action</th>
          </tr>
        );
        return columns; 
      }
  }

  GetUserConsentsWaivers(currentPage,pageSize,searchText){
    this.showLoader();

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    let canDelete = getrole.consents_waivers_can.consents_waivers_can_delete;
    /* Role Management */

    this.setState({ ListGrid : [] });

    // Pagination
    let bodyarray = {};
    bodyarray["currentPage"] = 1;
    bodyarray["nextPage"] = false;
    bodyarray["pageSize"] = 5;
    bodyarray["previousPage"] = false;
    bodyarray["totalCount"] = 0;
    bodyarray["totalPages"] = 0;
    
    this.setState({ pagingData : bodyarray });

    this.setState({ currentPage: currentPage });
    this.setState({ pageSize: pageSize });

    var sort_Column = this.state.sortColumn;
    var Sort_Type = this.state.SortType;
    
    var IsSortingEnabled = true;

    var url_paging_para = '&pageNumber='+currentPage+'&pageSize='+pageSize+'&searchText='+searchText+'&sortColumn='+sort_Column+'&SortType='+Sort_Type+'&IsSortingEnabled='+IsSortingEnabled;
    // Pagination

    var url=process.env.API_API_URL+'GetUserConsentsWaivers?contactId='+this.state.staffContactID+'&canDelete='+canDelete+url_paging_para;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        // console.log("responseJson GetUserConsentsWaivers");
        // console.log(data);
        // console.log(data.data.userRole);
        
        if (data.responseType === "1") {
            // Profile & Contact
            //this.setState({ ListGrid: data.data.userConsetsWaiverViews });
            this.setState({ header_data: this.TableHeaderDesign() });

            this.setState({ ListGrid: this.rowData(data.data.userConsetsWaiverViews) })
            this.setState({ ConsentType : data.data.consentType});

            this.setState({ pagingData: data.pagingData });

        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('GetUserConsentsWaivers error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }

  Edit_Update_Btn_Func(record){
    let return_push = [];

    if(this.state.role_consents_waivers_can.consents_waivers_can_update == true || this.state.role_consents_waivers_can.consents_waivers_can_delete == true){
      let Edit_push = [];
      if(this.state.role_consents_waivers_can.consents_waivers_can_update == true){
        Edit_push.push(
          <a href="#" onClick={this.EditRecord(record)} data-toggle="modal" data-target="#ConsentsAwards_awards_recognition_edit_modal"  className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
        );
      }
      let Delete_push = [];
      if(this.state.role_consents_waivers_can.consents_waivers_can_delete == true){
        if(record.isDelete == false)  
        {
          Delete_push.push(
            <a href="#" onClick={this.EditRecord(record)}   data-toggle="modal" data-target="#delete_consents_and_waivers" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Inactive</a>
          );
        }
        else
        {
          Delete_push.push(
            <a href="#" onClick={this.EditRecord(record)}   data-toggle="modal" data-target="#delete_consents_and_waivers" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Active</a>
          );
        }
      }
      
      return_push.push(
        <div className="dropdown dropdown-action">
          <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
          <div className="dropdown-menu dropdown-menu-right">
            {Edit_push}
            {Delete_push}
          </div>
        </div>
      );
    }
    return return_push;
  }

  EditRecord = (record) => e => {
    e.preventDefault();

    // console.log('edit Consents and Waivers');
    // console.log(record);

    this.setState({ errormsg: '' });

    this.setState({EditconsentId :  record.consentId });
    this.setState({Editname :  record.name });
    this.setState({EditissueDate : moment(record.issueDate,process.env.API_DATE_FORMAT) });
    this.setState({EditexpiryDate :  moment(record.expiryDate,process.env.API_DATE_FORMAT) });
    //this.setState({EditissueDate : moment(record.issueDate).format('YYYY-MM-DD') });
    //this.setState({EditexpiryDate :  moment(record.expiryDate).format('YYYY-MM-DD') });
    this.setState({Editdescription :  record.description });
    this.setState({Editstatus :  record.status });
    this.setState({Editnote :  record.note });
    this.setState({Editattachement :  record.attachement });
    this.setState({noteId :  record.noteId });
    //var createBase64 = "data:"+record.fileType+";base64,"+record.base64String;
    //this.setState({TempEditattachement :  createBase64 });
    this.setState({EditconsentType :  record.consentType });

    if(record.status == false){
      $('#Editstatus').val('false').trigger('change');
    }else{
      $('#Editstatus').val('true').trigger('change');
    }

    this.setState({ isDelete: record.isDelete });

    if(record.timeSheetNotesFile != null){
      // console.log('timeSheetNotesFile');

      let temp = [];
      var FileList = record.timeSheetNotesFile;
      for (var z = 0; z < FileList.length; z++)
      {
        temp.push({
          base64String: FileList[z].base64String,
          documentId: FileList[z].documentId,
          documentName: FileList[z].documentName,
          documentType: FileList[z].documentType,
          fileType: FileList[z].fileType,
          status: true
        });
      }

      // console.log('temp');
      // console.log(temp);
      this.setState({ filePreviewsFinalConsentsEditActive: FileList.length });
      this.setState({ filePreviewsFinalEdit: temp });
    }
   
    // console.log(moment(record.issueDate).format('YYYY-MM-DD'));
    
  }

  UpdateRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};
    

    if (this.state["Editname"] == "") {
      step1Errors["Editname"] = "Consent or Waiver Name is mandatory.";
    }

    if (this.state["EditissueDate"] == "" || this.state["EditissueDate"] == null) {
      step1Errors["EditissueDate"] = "Issue Date is mandatory";
    }

    /*if (this.state["EditexpiryDate"] == "" || this.state["EditexpiryDate"] == null) {
      step1Errors["EditexpiryDate"] = "Expiry Date mandatory";
    }*/

    if (this.state["Editdescription"] == "" || this.state["Editdescription"] == null) {
      step1Errors["Editdescription"] = "Description mandatory";
    }

    

    if (this.state["Editstatus"] == 'na') {
      step1Errors["Editstatus"] = "Please Select Status.";
    }

    var tempfile = '';
    // if(this.state.UpdatefilePreviewsFinal.length == 0){
    //   tempfile = this.state.TempEditattachement;
    // }else{
    //   tempfile = this.state.UpdatefilePreviewsFinal[0].FileData
    // }






    
    

    // console.log(step1Errors);

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }


    this.showLoader();

    var EditissueDateChangeFormate=moment(this.state["EditissueDate"]).format('MM-DD-YYYY');
    //var NewEditissueDate = moment(EditissueDateChangeFormate, "MM-DD-YYYY").add(1, 'days');
    
    var EditexpiryDate=moment(this.state["EditexpiryDate"]).format('MM-DD-YYYY');
    //var NewEditexpiryDate = moment(EditexpiryDate, "MM-DD-YYYY").add(1, 'days');

    // var base64result ='';
    // var fileType ='';
    
    // if(this.state.UpdatefilePreviewsFinal.length != 0)
    // {
    //   var file = tempfile;
    //   var fileType = file.match(/[^:]\w+\/[\w-+\d.]+(?=;|,)/)[0];
    //   var base64result = file.split(',')[1];
    // }
  
    //alert(this.state["EditconsentType"]);


    var isFileAttached = false;
    let File_Push = [];
    if(this.state.UpdatefilePreviewsFinal.length > 0)
    {
      // var file = this.state.filePreviewsFinal[0].FileData;
      // var fileType = file.match(/[^:]\w+\/[\w-+\d.]+(?=;|,)/)[0];
      // var base64result = file.split(',')[1];
      var isFileAttached = true;
      
      var FileUpload = this.state.UpdatefilePreviewsFinal;
      for (var z = 0; z < FileUpload.length; z++)
      {
        var file = FileUpload[z].FileData;
        var fileType = file.match(/[^:]\w+\/[\w-+\d.]+(?=;|,)/)[0];
        var base64result = file.split(',')[1];

        let File = {};
        File.FileType = fileType;
        File.Base64String = base64result;
        File.DocumentType = 'Consent & Waiver';
        File.documentName = FileUpload[z].FileName;
        File.documentId = null;
        File.status = true;
        File_Push.push(File);
      }
    }

    if(this.state.filePreviewsFinalEdit.length > 0){
      var isFileAttached = true;
      
      var FileUploadEdit = this.state.filePreviewsFinalEdit;

      for (var j = 0; j < FileUploadEdit.length; j++)
      {
        if(FileUploadEdit[j].status == false){

          let File = {};
          File.base64String = FileUploadEdit[j].base64String;
          File.documentId = FileUploadEdit[j].documentId;
          File.documentName = FileUploadEdit[j].documentName;
          File.documentType = FileUploadEdit[j].documentType;
          File.fileType = FileUploadEdit[j].fileType;
          File.status = FileUploadEdit[j].status;
          File_Push.push(File);

        }
        
      }

    }

    // console.log(File_Push);
    
    let ArrayJson = {
          consentId: this.state["EditconsentId"],
          name: this.state["Editname"],
          issueDate: EditissueDateChangeFormate,
          expiryDate: EditexpiryDate,
          description: this.state["Editdescription"],
          status: this.state["Editstatus"],
          note: this.state["Editnote"],
          noteId: this.state["noteId"],
          consentType: this.state["EditconsentType"],
          //base64String: base64result,
          //fileType:fileType,
          TimeSheetNotesFile: File_Push

    };
     
    let bodyarray = {};
    bodyarray["contactId"] = this.state.staffContactID;
    bodyarray["userConsetsWaiver"] = ArrayJson;
    bodyarray["userName"] = this.state.staffContactFullname;

    // console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'UpdateUserConsentsWaivers';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        // console.log("responseJson UpdateUserConsentsWaivers");
        // console.log(data);
        // console.log(responseJson);
        
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');
            this.setState({ EditResetflag : true });

            setTimeout(
                function() {
                    this.setState({ EditResetflag: false });
                }
                .bind(this),
                2000
            );
            //setTimeout(this.setState({ EditResetflag : false }), 3000);

            this.setState({ filePreviewsFinalEdit: [] });
            this.setState({ filePreviewsFinalConsentsEditActive: 0 });

            this.setState({ UpdatefilePreviewsFinal: [] });


            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".close" ).trigger( "click" );

            this.setState({ Editattachement : '' });

            
            
            this.GetUserConsentsWaivers(this.state.currentPage,this.state.pageSize,this.state.searchText);
               
        }
        else if (data.responseType === "2") {
            SystemHelpers.ToastError(data.responseMessge);
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('UpdateUserConsentsWaivers error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }

  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ Addname: '' });
    this.setState({ AddissueDate: '' });
    this.setState({ AddexpiryDate: '' });
    this.setState({ Adddescription: '' });
    this.setState({ Addstatus: 'na' });
    this.setState({ Addnote: '' });
    this.setState({ filePreviewsFinal: '' });
    this.setState({ Addattachment: '' });
    this.setState({ AddconsentType : '' });
    //this.setState({ ConsentType : [] });

    this.setState({ AddResetflag : true });

    setTimeout(
        function() {
            this.setState({ AddResetflag: false });
        }
        .bind(this),
        2000
    );
    //setTimeout(this.setState({ AddResetflag : false }), 3000);

    this.setState({ errormsg: '' });
  }

  AddRecord = () => e => {
    
    // console.log(this.state.filePreviewsFinal);

    e.preventDefault();

    let step1Errors = {};
    

    if (this.state["Addname"] == "") {
      step1Errors["Addname"] = "Consent or Waiver Name is mandatory.";
    }

    if (this.state["AddissueDate"] == "" || this.state["AddissueDate"] == null) {
      step1Errors["AddissueDate"] = "Issue Date is mandatory";
    }

    /*if (this.state["AddexpiryDate"] == "" || this.state["AddexpiryDate"] == null) {
      step1Errors["AddexpiryDate"] = "Expiry Date mandatory";
    }*/

    if (this.state["Adddescription"] == "" || this.state["Adddescription"] == null) {
      step1Errors["Adddescription"] = "Description mandatory";
    }

    
    if (this.state["Addstatus"] == 'na') {
      step1Errors["Addstatus"] = "Please Select Status.";
    }


    if(this.state.filePreviewsFinal.length == 0){
        step1Errors["Addattachment"] = "Please Select Attachment File.";
    }

   

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }
    
    
    

    this.showLoader();
    var AddissueDateChangeFormate=moment(this.state["AddissueDate"]).format('MM-DD-YYYY');
    var AddexpiryDateChangeFormate=moment(this.state["AddexpiryDate"]).format('MM-DD-YYYY');

    // if(this.state.filePreviewsFinal.length > 0)
    // {
    //   var file = this.state.filePreviewsFinal[0].FileData;
    //   var fileType = file.match(/[^:]\w+\/[\w-+\d.]+(?=;|,)/)[0];
    //   var base64result = file.split(',')[1];
    // }


    // console.log('filePreviewsFinal');
    // console.log(this.state.filePreviewsFinal);

    var isFileAttached = false;
    let File_Push = [];
    if(this.state.filePreviewsFinal.length > 0)
    {
      // var file = this.state.filePreviewsFinal[0].FileData;
      // var fileType = file.match(/[^:]\w+\/[\w-+\d.]+(?=;|,)/)[0];
      // var base64result = file.split(',')[1];
      var isFileAttached = true;
      
      var FileUpload = this.state.filePreviewsFinal;
      for (var z = 0; z < FileUpload.length; z++)
      {
        var file = FileUpload[z].FileData;
        var fileType = file.match(/[^:]\w+\/[\w-+\d.]+(?=;|,)/)[0];
        var base64result = file.split(',')[1];

        let File = {};
        File.FileType = fileType;
        File.Base64String = base64result;
        File.DocumentType = 'Consent & Waiver';
        File.documentName = FileUpload[z].FileName;
        File.documentId = null;
        File.status = true;
        File_Push.push(File);
      }
      // console.log(File_Push);
    }

    let ArrayJson = {
          name: this.state["Addname"],

          //issueDate: moment(AddissueDateChangeFormate, "MM-DD-YYYY").add(1, 'days'),
          issueDate: AddissueDateChangeFormate,

          //expiryDate: moment(AddexpiryDateChangeFormate, "MM-DD-YYYY").add(1, 'days'),
          expiryDate: AddexpiryDateChangeFormate,

          description: this.state["Adddescription"],
          status: this.state["Addstatus"],
          note: this.state["Addnote"],
          //base64String: base64result,
          //fileType:fileType,
          TimeSheetNotesFile: File_Push,
          consentType: this.state["AddconsentType"]
    };
     
    let bodyarray = {};
    bodyarray["contactId"] = this.state.staffContactID;
    bodyarray["userConsetsWaiver"] = ArrayJson;

    

    // console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'CreateUserConsentsWaivers';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        // console.log("responseJson CreateUserConsentsWaivers");
        // console.log(data);
        // console.log(responseJson);
        
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');

            this.setState({ Addname: '' });
            this.setState({ AddissueDate: '' });
            this.setState({ AddexpiryDate: '' });
            this.setState({ Adddescription: '' });
            this.setState({ Addstatus: 'na' });
            this.setState({ Addnote: '' });
            this.setState({ filePreviewsFinal: '' });
            this.setState({ Addattachment: '' });
            this.setState({ AddconsentType : '' });
            this.setState({ ConsentType : [] });

            this.setState({ AddResetflag : true });

            setTimeout(
                function() {
                    this.setState({ AddResetflag: false });
                }
                .bind(this),
                500
            );
            
            
            
            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".close" ).trigger( "click" );
            this.GetUserConsentsWaivers(this.state.currentPage,this.state.pageSize,this.state.searchText);
               
        }
        else if (data.responseType === "2") {
            SystemHelpers.ToastError(data.responseMessge);
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('CreateUserConsentsWaivers error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }

  DeleteRecord = () => e => {
    e.preventDefault();

    var isdelete = '';
    if(this.state.isDelete== true)
    {
      isdelete = false;
    }
    else
    {
      isdelete = true;
    }

    this.showLoader();
    
    var url=process.env.API_API_URL+'DeleteUserConsentsWaivers?consentsId='+this.state.EditconsentId+'&isDelete='+isdelete+'&userName='+this.state.staffContactFullname;
    fetch(url, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        // console.log("responseJson DeleteUserConsentsWaivers");
        // console.log(data);
        // console.log(data.data.userRole);
        
        if (data.responseType === "1") {
            // Profile & Contact
            SystemHelpers.ToastSuccess(data.responseMessge);


            $( ".cancel-btn" ).trigger( "click" );
            this.GetUserConsentsWaivers(this.state.currentPage,this.state.pageSize,this.state.searchText);
            this.hideLoader();
        }else if (data.responseType == "2" || data.responseType == "3") {
            SystemHelpers.ToastError(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.hideLoader();
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
              }else{
                SystemHelpers.ToastError(data.message);
              }
              this.hideLoader();
              $( ".cancel-btn" ).trigger( "click" );
        }
        
        
    })
    .catch(error => {
      console.log('DeleteUserConsentsWaivers error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }

  rowData(ListGrid) {
    // console.log(userList)

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    let canDelete = getrole.consents_waivers_can.consents_waivers_can_delete;
    /* Role Management */

      var ListGrid_length = ListGrid.length;
      let dataArray = [];
      var i=1;
      for (var z = 0; z < ListGrid_length; z++) {
        var tempdataArray = [];
        
        var issueDate = "";
        if(ListGrid[z].issueDate == "" || ListGrid[z].issueDate == null){
          issueDate = "";
        }else{
          issueDate = moment(ListGrid[z].issueDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT);
        }

        var expiryDate ="";
        if(ListGrid[z].expiryDate == "" || ListGrid[z].expiryDate == null){
          expiryDate = "";
        }else{
          expiryDate = moment(ListGrid[z].expiryDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT);
        }

        var download = "";
        var FileList = ListGrid[z].timeSheetNotesFile;
        if(FileList.length > 0)
        {
          download = <a href="#" onClick={this.EditRecord(ListGrid[z])}  data-toggle="modal" data-target="#ConsentsAwards_awards_recognition_attachment_modal"  className="btn btn-danger btn-sm mr-1"><i className="fa fa-paperclip m-r-5" ></i> View</a>
        }

        var status_delete = "";
        if(canDelete == true)
        {
          if(ListGrid[z].isDelete == true){
            status_delete = <div><span class="badge bg-inverse-warning">Inactive</span></div>;
          }else{
            status_delete = <div><span class="badge bg-inverse-success">Active</span></div>;
          }
        }

        tempdataArray.push(<tr>
          <td>{ListGrid[z].name}</td>
          <td>{issueDate}</td>
          <td>{expiryDate}</td>
          <td>{ListGrid[z].status == true ? "Active" : "Expired"}</td>
          <td>{ListGrid[z].consentType}</td>
          <td>{download}</td>
          <td>{status_delete}</td>
          
          <td>{this.Edit_Update_Btn_Func(ListGrid[z])}</td>
        </tr>);

        dataArray.push(tempdataArray);
        i++;
      }

      return dataArray;
  }

  DeleteImg = (documentId) => e => {
      e.preventDefault();
      // console.log(documentId);
      if(confirm("Are you sure you want to delete this?"))
      {
        let temp = [];
        var FileList = this.state.filePreviewsFinalEdit;
        var active = 0;
        for (var z = 0; z < FileList.length; z++)
        {
          if(FileList[z].documentId == documentId){
            temp.push({
              base64String: FileList[z].base64String,
              documentId: FileList[z].documentId,
              documentName: FileList[z].documentName,
              documentType: FileList[z].documentType,
              fileType: FileList[z].fileType,
              status: false
            });
          }else{
            temp.push({
              base64String: FileList[z].base64String,
              documentId: FileList[z].documentId,
              documentName: FileList[z].documentName,
              documentType: FileList[z].documentType,
              fileType: FileList[z].fileType,
              status: FileList[z].status
            });

            active++;
            // if(FileList[z].status){
            //   active++;
            // }
          }

        }

        //var current_upload1=$("#filePreviewsFinalConsentsEditActive").val();
        //$("#filePreviewsFinalConsentsEditActive").val((parseInt(current_upload)-1));
        //alert(active);
        this.setState({ filePreviewsFinalConsentsEditActive: active });
        this.setState({ filePreviewsFinalEdit: temp });
        return false;

      }else{
        return false;
      }   
  }

  NewTabOpen = (baseurl) => e => {
      e.preventDefault();
      // console.log(baseurl);
      
      const linkSource = baseurl;
      const downloadLink = document.createElement("a");
  

      downloadLink.href = linkSource;
      downloadLink.download = this.GetFileName(baseurl);
      downloadLink.click();
      return false;
  }

  GetImageAll(baseurl){
      var imgbase=baseurl;
      var gettype=this.base64MimeType(baseurl);

      let Image_return = [];

      if(gettype == 'image/png' || gettype == 'image/jpeg' || gettype == 'image/jpg' ){
          Image_return.push(<img src={baseurl} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />);
      } else if(gettype == 'application/pdf'){
          Image_return.push(<img src={PdfImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />);
      } else if(gettype == 'application/vnd.ms-excel'){
          Image_return.push(<img src={ExcelImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />);
      } else if(gettype == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
          Image_return.push(<img src={ExcelImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />);
      } else if(gettype == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || gettype == 'application/msword'){
          Image_return.push(<img src={DocxImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />);
      }

      return Image_return;
  }

  base64MimeType(encoded) {
    var result = null;

    if (typeof encoded !== 'string') {
      return result;
    }

    var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

    if (mime && mime.length) {
      result = mime[1];
    }

    return result;
  }

  GetImageAllNewImg(ImgDetails){
      var FileNm = ImgDetails.documentName;
      var FileUrl = ImgDetails.base64String;
      var gettype = FileNm.substr( (FileNm.lastIndexOf('.') +1));

      // console.log(gettype);
      // var imgbase=baseurl;
      // var gettype=this.base64MimeType(baseurl);

      let Image_return = [];
      let Image_return_Final = [];

      if(gettype == 'png' || gettype == 'jpeg' || gettype == 'jpg' ){
          var image_var = <img src={FileUrl} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } else if(gettype == 'pdf'){
          var image_var = <img src={PdfImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } else if(gettype == 'csv' || gettype == 'xlsx' || gettype == 'xls' ){
          var image_var = <img src={ExcelImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      }  else if(gettype == 'docx' || gettype == 'doc'){
          var image_var = <img src={DocxImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } 

      if(ImgDetails.status == true){
        Image_return_Final.push(
            <div className="col-6 col-sm-4 col-md-3 col-lg-4 col-xl-3">
                <div className="card card-file">
                  <div className="dropdown-file">
                    <a href="" className="dropdown-link" data-toggle="dropdown"><i className="fa fa-ellipsis-v" /></a>
                    <div className="dropdown-menu dropdown-menu-right">
                      <a href={FileUrl} target="_blank" className="dropdown-item">Download</a>
                      <a href="#" onClick={this.DeleteImg(ImgDetails.documentId)} className="dropdown-item">Delete</a>
                    </div>
                  </div>
                  <div className="card-file-thumb">
                    {image_var}
                  </div>
                </div>
            </div>
        );
      }



      return Image_return_Final;
  }

  GetImageAllNewImgView(ImgDetails){

      //alert();

      var FileNm = ImgDetails.documentName;
      var FileUrl = ImgDetails.base64String;
      var gettype = FileNm.substr( (FileNm.lastIndexOf('.') +1));

      // console.log(ImgDetails);
      // var imgbase=baseurl;
      // var gettype=this.base64MimeType(baseurl);

      let Image_return = [];
      let Image_return_Final = [];

      if(gettype == 'png' || gettype == 'jpeg' || gettype == 'jpg' ){
          var image_var = <img src={FileUrl} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } else if(gettype == 'pdf'){
          var image_var = <img src={PdfImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } else if(gettype == 'csv' || gettype == 'xlsx' || gettype == 'xls' ){
          var image_var = <img src={ExcelImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      }  else if(gettype == 'docx' || gettype == 'doc'){
          var image_var = <img src={DocxImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } 

      if(ImgDetails.status == true){
        Image_return_Final.push(
            <div className="col-6 col-sm-4 col-md-3 col-lg-4 col-xl-3">
                <div className="card card-file">
                  <div className="dropdown-file">
                    <a href="" className="dropdown-link" data-toggle="dropdown"><i className="fa fa-ellipsis-v" /></a>
                    <div className="dropdown-menu dropdown-menu-right">
                      <a href={FileUrl} target="_blank" className="dropdown-item">Download</a>
                      {/*<a href="#" onClick={this.DeleteImg(ImgDetails.documentId)} className="dropdown-item">Delete</a>*/}
                    </div>
                  </div>
                  <div className="card-file-thumb">
                    {image_var}
                  </div>
                </div>
            </div>
        );
      }



      return Image_return_Final;
  }

  GetImageAllNew(baseurl){
      var imgbase=baseurl;
      var gettype=this.base64MimeType(baseurl);

      let Image_return = [];
      let Image_return_Final = [];

      if(gettype == 'image/png' || gettype == 'image/jpeg' || gettype == 'image/jpg' ){
          var image_var = <img src={baseurl} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } else if(gettype == 'application/pdf'){
          var image_var = <img src={PdfImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } else if(gettype == 'application/vnd.ms-excel'){
          var image_var = <img src={ExcelImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } else if(gettype == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
          var image_var = <img src={ExcelImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } else if(gettype == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || gettype == 'application/msword'){
          var image_var = <img src={DocxImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } 

      Image_return_Final.push(
          <div className="col-6 col-sm-4 col-md-3 col-lg-4 col-xl-3">
              <div className="card card-file">
                {/*<div className="dropdown-file">
                  <a href="" className="dropdown-link" data-toggle="dropdown"><i className="fa fa-ellipsis-v" /></a>
                  <div className="dropdown-menu dropdown-menu-right">
                    <a href="#" onClick={this.NewTabOpen(baseurl)} className="dropdown-item">Download</a>
                    <a href="#" className="dropdown-item">Delete</a>
                  </div>
                </div>*/}
                <div className="card-file-thumb">
                  {image_var}
                </div>
              </div>
          </div>
      );



      return Image_return_Final;
  }

  // Pagination Design
  PaginationDesign ()
  {
    let PageOutput = [];
    // console.log('pagination');
    // console.log(this.state.pagingData);
    
    if(this.state.pagingData !="" && this.state.pagingData !="undefined")
    {
      var Page_Count = this.state.pagingData.totalPages;
      //alert(this.state.pagingData.currentPage);
      // console.log('page count = ' + Page_Count);
      /* pagination count */

      var Page_Start=1;
      var Page_End=1;

      if(this.state.pagingData.currentPage == 1){
        Page_Start=1;

        if(Page_Count <= 10){
          Page_End=Page_Count;
        }else{
          Page_End=10;
        }
      }else{
        if(this.state.pagingData.currentPage < 5){
          Page_Start=1;
          Page_End=Page_Count;
          //Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
          // console.log("Page_End 1 "+ Page_End);
        }else{
          Page_Start=parseInt(this.state.pagingData.currentPage) - parseInt(4);
          Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
          // console.log("Page_End 2 "+ Page_End);
          if(Page_End > Page_Count){
            Page_End=Page_Count;
            // console.log("Page_End 3 "+ Page_End);
          }
        }
      }

      let Page = [];
      var i = 1;
      for (var z=Page_Start; z <= Page_End ; z++)
      {
        if(z==this.state.pagingData.currentPage)
        {
          Page.push(<li className="page-item active pk-active">
            <a className="page-link pk-active" id={z} href="#" onClick={this.PageGetGridData}>{z}<span className="sr-only">(current)</span></a>
          </li>);
        }
        else
        {
          Page.push(<li className="page-item"><a className="page-link" id={z} href="#" onClick={this.PageGetGridData} >{z}</a></li>);
        }
        i++;
      }

      let PagePrev = [];

      if(this.state.pagingData.currentPage == 1){
        PagePrev.push(<li className="page-item disabled">
          <a className="page-link" href="#">Previous</a>
        </li>);
      }else{
        PagePrev.push(<li className="page-item">
          <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)-parseInt(1)} tabIndex={-1} onClick={this.PageGetGridData}>Previous</a>
        </li>);
      }

      let PageNext = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageNext.push(<li className="page-item disabled">
          <a className="page-link" href="#">Next</a>
        </li>);
      }else{
        PageNext.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)+parseInt(1)} onClick={this.PageGetGridData}>Next</a>
          </li>
        );
      }

      let PageLast = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageLast.push(<li className="page-item disabled">
          <a className="page-link" href="#">Last</a>
        </li>);
      }else{
        PageLast.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(Page_Count)} onClick={this.PageGetGridData}>Last</a>
          </li>
        );
      }
      
      PageOutput.push(<section className="comp-section" id="comp_pagination">
                        <div className="pagination-box">
                          <div>
                            <ul className="pagination">
                              
                              {PagePrev}
                              {Page}
                              {PageNext}
                              {PageLast}
                              
                            </ul>
                          </div>
                        </div>
                      </section>);
    }
    
    return PageOutput;
  }

  PageGetGridData = e => {

    e.preventDefault();
    let current_page= e.target.id;
    this.GetUserConsentsWaivers(current_page,this.state.pageSize,this.state.searchText)
  }

  SearchGridData = e => {
    this.setState({ pageSize: this.state.TempsearchText });
    this.GetUserConsentsWaivers(1,this.state.pageSize,this.state.TempsearchText);
  }
  // Pagination Design

  render() {
     const { Editname, EditissueDate, EditexpiryDate, Editdescription, Editstatus, Editnote } = this.props; 
     
      return (
        <div>
            {/* Toast & Loder method use */}
            
            {(this.state.loading) ? <Loader /> : null} 
            {/* Toast & Loder method use */}
        <div className="row">
          <div className="col-md-12 d-flex">
            <div className="card profile-box flex-fill">

              <div className="row">
                <button className="btn btn-primary submit-btn pk-profiletab-refreshbtn-hide" id="TabClickOnLoadConsentsWaivers" onClick={this.TabClickOnLoadConsentsWaivers()}>Refresh</button>
              </div>

              <div className="card-body">
                {this.state.role_consents_waivers_can.consents_waivers_can_create == true ?
                  <h3 className="card-title">Consents and Waivers<a href="#" className="edit-icon" data-toggle="modal" data-target="#ConsentsAwards_awards_recognition_Add_modal"><i className="fa fa-plus" /></a></h3>
                  : <h3 className="card-title">Consents and Waivers <a href="#" className="phss-lock"><i className="fa fa-lock" /></a></h3>
                }

                {/* Page Per Record and serach design*/}
                  <div className="row filter-row">
                    <div className="col-sm-6 col-md-2"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" value={this.state.pageSize}  onChange={this.handleChange('pageSize')}> 
                          <option value="5">5/Page</option>
                          <option value="10">10/Page</option>
                          <option value="50">50/Page</option>
                          <option value="100">100/Page</option>
                        </select>
                        <label className="focus-label">Per Page</label>
                      </div>
                    </div>
                    
                    <div className="col-sm-6 col-md-3">
                      <div className="form-group form-focus focused">
                        <label className="focus-label">Sorting</label>
                        <select className="form-control floating" id="sortColumn" value={this.state.sortColumn} onChange={this.handleChange('sortColumn')}> 
                          {/*<option value="">-</option>*/}
                          <option value="IssueDate">Issue Date</option>
                          <option value="ExpiryDate">Expiry Date</option>
                          <option value="ConsentType">Consent Type</option>
                        </select>
                      </div>
                    </div> 

                    <div className="col-sm-6 col-md-2">
                      <div className="form-group form-focus focused">
                        <label className="focus-label">Sorting Order</label>
                        <select className="form-control floating" id="SortTypeId" value={this.state.SortType} onChange={this.handleChange('SortType')}> 
                          {/*<option value="">-</option>*/}
                          <option value="false">Ascending</option>
                          <option value="true">Descending</option>
                        </select>
                      </div>
                    </div> 

                    <div className="col-sm-6 col-md-3">  
                      <div className="form-group form-focus focused">
                        <input className="form-control floating" type="text" value={this.state.TempsearchText}  onChange={this.handleChange('TempsearchText')} placeholder="search by Consent Name" />
                        <label className="focus-label">Search</label>
                      </div>
                    </div>

                    <div className="col-sm-6 col-md-2">  
                      <a href="#" className="btn btn-success btn-block" onClick={this.SearchGridData}> Search </a>  
                    </div> 
                  </div>
                {/* Page Per Record and serach design*/}
                
                <div className="row">
                  <div className="col-md-12">
                    <div className="table-responsive">
                      
                      <table className="table table-striped custom-table mb-0 datatable">
                        <thead>
                          { this.state.ListGrid.length > 0 ? this.state.header_data : null}
                        </thead>
                        <tbody>
                          
                          {this.state.ListGrid.length > 0 ? this.state.ListGrid : null}
                        </tbody>
                       </table> 

                      {this.PaginationDesign()}

                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
        {/* ************ Consents and Waivers Tab Modals ************ */}
          {/* Consents and Waivers Add Modal */}
          <div id="ConsentsAwards_awards_recognition_Add_modal" className="modal custom-modal fade" role="dialog">
            <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title">Consents and Waivers</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()} >
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div className="modal-body">
                  <form>
                    <div className="card">
                      <div className="card-body">
                        <div className="row">
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Name<span className="text-danger">*</span></label>
                              <input className="form-control" type="text" value={this.state.Addname} onChange={this.handleChange('Addname')} />
                              <span className="form-text error-font-color">{this.state.errormsg["Addname"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Status<span className="text-danger">*</span></label>
                              <select value={this.state.Addstatus} className="form-control" id="Addstatus" onChange={this.handleChange('Addstatus')}>
                                  <option value="na">-</option>
                                  <option value="true">Active</option>
                                  <option value="false">Expired</option>
                                </select>
                              <span className="form-text error-font-color">{this.state.errormsg["Addstatus"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Issue Date<span className="text-danger">*</span></label>
                                <Datetime
                                  inputProps={{readOnly: true}}
                                  closeOnTab={true}
                                  input={true}
                                  value={(this.state.AddissueDate) ? this.state.AddissueDate : ''}
                                  onChange={this.handleAddissueDate}
                                  dateFormat={process.env.DATE_FORMAT}
                                  timeFormat={false}
                                  renderInput={(props) => {
                                     return <input {...props} value={(this.state.AddissueDate) ? props.value : ''} />
                                  }}
                                />
                                {/*<input className="form-control" type="date" value={this.state.AddissueDate} onChange={this.handleChange('AddissueDate')}  />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["AddissueDate"]}</span>
                              </div>
                            
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Expiry Date</label>
                                <Datetime
                                isValidDate={this.validationAddexpiryDate}
                                inputProps={{readOnly: true}}
                                closeOnTab={true}
                                input={true}
                                value={(this.state.AddexpiryDate) ? this.state.AddexpiryDate : ''}
                                onChange={this.handleAddexpiryDate}
                                dateFormat={process.env.DATE_FORMAT}
                                timeFormat={false}
                                renderInput={(props) => {
                                   return <input {...props} value={(this.state.AddexpiryDate) ? props.value : ''} />
                                }}
                              />
                                {/*<input className="form-control" type="date" value={this.state.AddexpiryDate} onChange={this.handleChange('AddexpiryDate')} min={moment().format(this.state.AddissueDate,"YYYY-MM-DD")} />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["AddexpiryDate"]}</span>
                              </div>
                            
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Description<span className="text-danger">*</span></label>
                              <textarea rows="5" cols="5" className="form-control" placeholder="Enter text here" value={this.state.Adddescription} onChange={this.handleChange('Adddescription')}></textarea>
                              <span className="form-text error-font-color">{this.state.errormsg["Adddescription"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Notes</label>
                              {/*<input className="form-control" type="text" value={this.state.Addnote} onChange={this.handleChange('Addnote')} />*/}
                              <textarea rows="5" cols="5" class="form-control" value={this.state.Addnote} onChange={this.handleChange('Addnote')}></textarea>
                              <span className="form-text error-font-color">{this.state.errormsg["Addnote"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Consent Type</label>
                              <select className="form-control" value={this.state.AddconsentType} onChange={this.handleChange('AddconsentType')}>
                                  <option value="">-</option>
                                  {this.state.ConsentType.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["AddconsentType"]}</span>
                            </div>
                          </div>
                          <div className="col-md-12">
                            <div className="form-group">
                              <label>Attachment<span className="text-danger">*</span></label>
                              <FileUploadPreview
                                className="form-control"
                                ResetFileMethod={this.state.AddResetflag}
                                setPropState={this.setPropState} />
                              
                              <span className="form-text success-font-color Guidelines_Doc">{process.env.ATTACHMENT_GUIDELINES}</span>
                              <span className="form-text error-font-color">{this.state.errormsg["Addattachment"]}</span>
                            </div>
                          </div>
                        </div>
                        <div className="submit-section">
                          <button className="btn btn-primary submit-btn" onClick={this.AddRecord()}>Submit</button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          {/* //Consents and Awards Add Modal */}
          {/* Consents and Waivers Edit Modal */}
          <div id="ConsentsAwards_awards_recognition_edit_modal" className="modal custom-modal fade" role="dialog">
            <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title">Consents and Waivers</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div className="modal-body">
                  <form>
                    <div className="card">
                      <div className="card-body">
                        <div className="row">
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Name<span className="text-danger">*</span></label>
                              <input className="form-control" type="text" value={this.state.Editname} onChange={this.handleChange('Editname')} />
                              <span className="form-text error-font-color">{this.state.errormsg["Editname"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Status<span className="text-danger">*</span></label>
                              <select value={this.state.Editstatus} className="form-control" id="Editstatus" value={this.state.Editstatus} onChange={this.handleChange('Editstatus')}>
                                  <option value="na">-</option>
                                  <option value="true">Active</option>
                                  <option value="false">Expired</option>
                                </select>
                              <span className="form-text error-font-color">{this.state.errormsg["Editstatus"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Issue Date<span className="text-danger">*</span></label>
                                <Datetime
                                  inputProps={{readOnly: true}}
                                  closeOnTab={true}
                                  input={true}
                                  value={(this.state.EditissueDate) ? this.state.EditissueDate : ''}
                                  onChange={this.handleEditissueDate}
                                  dateFormat={process.env.DATE_FORMAT}
                                  timeFormat={false}
                                  renderInput={(props) => {
                                     return <input {...props} value={(this.state.EditissueDate) ? props.value : ''} />
                                  }}
                                />
                                {/*<input className="form-control" type="date" value={this.state.EditissueDate} onChange={this.handleChange('EditissueDate')}  />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["EditissueDate"]}</span>
                              </div>
                            
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Expiry Date</label>
                                <Datetime
                                  inputProps={{readOnly: true}}
                                  isValidDate={this.validationEditexpiryDate}
                                  closeOnTab={true}
                                  input={true}
                                  value={(this.state.EditexpiryDate) ? this.state.EditexpiryDate : ''}
                                  onChange={this.handleEditexpiryDate}
                                  dateFormat={process.env.DATE_FORMAT}
                                  timeFormat={false}
                                  renderInput={(props) => {
                                     return <input {...props} value={(this.state.EditexpiryDate) ? props.value : ''} />
                                  }}
                                />
                                {/*<input className="form-control" type="date" value={this.state.EditexpiryDate} onChange={this.handleChange('EditexpiryDate')} min={moment().format(this.state.EditissueDate,"YYYY-MM-DD")} />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["EditexpiryDate"]}</span>
                              </div>
                            
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Description<span className="text-danger">*</span></label>
                              <textarea rows="5" cols="5" className="form-control" placeholder="Enter text here" value={this.state.Editdescription} onChange={this.handleChange('Editdescription')}></textarea>
                              <span className="form-text error-font-color">{this.state.errormsg["Editdescription"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Notes</label>
                              {/*<input className="form-control" type="text" value={this.state.Editnote} onChange={this.handleChange('Editnote')} />*/}
                              <textarea rows="5" cols="5" class="form-control" value={this.state.Editnote} onChange={this.handleChange('Editnote')}></textarea>
                              <span className="form-text error-font-color">{this.state.errormsg["Editnote"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Consent Type</label>
                              <select className="form-control" value={this.state.EditconsentType} onChange={this.handleChange('EditconsentType')}>
                                  <option value="">-</option>
                                  {this.state.ConsentType.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["EditconsentType"]}</span>
                            </div>
                          </div>
                          <div className="col-md-12">
                            <div className="form-group">
                              <label>Attachment<span className="text-danger">*</span></label>
                              <UpdateFileUploadPreview 
                              EditResetFileMethod={this.state.EditResetflag}
                              className="form-control" 
                              setPropState={this.setPropState} />
                              <input type="hidden" name="filePreviewsFinalConsentsEditActive" id="filePreviewsFinalConsentsEditActive" value={this.state.filePreviewsFinalConsentsEditActive}/>
                              <span className="form-text success-font-color Guidelines_Doc">{process.env.ATTACHMENT_GUIDELINES}</span>
                              <span className="form-text error-font-color">{this.state.errormsg["Editattachment"]}</span>
                            </div>
                          </div>

                          <div className="col-md-12">
                            {
                                    this.state.UpdatefilePreviewsFinal.length > 0 || this.state.filePreviewsFinalConsentsEditActive > 0 ?

                                    <div>
                                        <h4>Preview Files</h4>
                                        <br/>
                                        <div className="row row-sm">

                                            {this.state.filePreviewsFinalEdit.map(( listValue2, index ) => {
                                              return (
                                                this.GetImageAllNewImg(listValue2)
                                              );
                                            })}

                                            {this.state.UpdatefilePreviewsFinal.map(( listValue, index ) => {
                                              return (
                                                this.GetImageAllNew(listValue.FileData)
                                              );
                                            })}
                                            
                                        </div> 
                                    </div>: undefined
                                }  
                              </div>
                        </div>
                        <div className="submit-section">
                          <button className="btn btn-primary submit-btn" onClick={this.UpdateRecord()}>Update</button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          {/* //Consents and Awards Edit Modal */}

          {/* Consents and Awards Attachment Modal */}
            <div id="ConsentsAwards_awards_recognition_attachment_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Consents and Awards</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            

                            
                            <div className="col-md-12">
                            {
                                    this.state.UpdatefilePreviewsFinal.length > 0 || this.state.filePreviewsFinalConsentsEditActive > 0 ?

                                    <div>
                                        <h4>Attachment Files</h4>
                                        <br/>
                                        <div className="row row-sm">

                                            {this.state.filePreviewsFinalEdit.map(( listValue2, index ) => {
                                              return (
                                                this.GetImageAllNewImgView(listValue2)
                                              );
                                            })}

                                            
                                            
                                        </div> 
                                    </div>: undefined
                                }  
                              </div>

                          </div>
                          
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          {/* //Consents and Awards Attachment Modal */}
          {/* Delete Trained Locations  Modal */}
            <div className="modal custom-modal fade" id="delete_consents_and_waivers" role="dialog">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-body">
                    <div className="form-header">
                      <h3>Consents and Waivers</h3>
                      <p>Are you sure you want to mark consents and waivers as {this.state.isDelete == true ? 'Active' : 'Inactive' } ?</p>
                    </div>
                    <div className="modal-btn delete-action">
                      <div className="row">
                        <div className="col-6">
                          <a  onClick={this.DeleteRecord()} className="btn btn-primary continue-btn">{this.state.isDelete == true ? 'Active' : 'Inactive' }</a>
                        </div>
                        <div className="col-6">
                          <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">Cancel</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        {/* /Delete Trained Locations Modal */}
        
        {/* ************ Consents and Waivers Tab Modals ************ */}
        </div>
      );
   }
}

export default ConsentsAndWaivers;
