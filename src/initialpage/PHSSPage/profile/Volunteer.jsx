
/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";
//import {Avatar_02,Avatar_05,Avatar_09,Avatar_10,Avatar_16 } from '../../../../../Entryfile/imagepath'
import moment from 'moment';


import Loader from '../../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import SystemHelpers from '../../Helpers/SystemHelper';

class PhssEmploymentInformation extends Component {
  constructor(props) {
    super(props);

    this.state = {
        errormsg :  '',
        user_role: [],
        staffContactID:this.props.staffContactID,
        all_data :this.props.all_data,

        volunteerStatusList:[],
        volunteerTypeList:[],
        volunteerHoursList:[],

        refereedName : '',
        volunteeringstatus : '',
        volunteeringtype : '',
        volunteeringhours : '',

        EditrefereedName : '',
        Editvolunteeringstatus : '',
        Editvolunteeringtype : '',
        Editvolunteeringhours : '',

        role_volunteer_can:{},

        staffContactFullname : localStorage.getItem('fullName')
    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = input => e => {
    this.setState({ [input]: e.target.value.replace(/^\s+/g, '') });
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

   // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  // toast hide show method
  ToastSuccess(msg){
      toast.success(msg, {
        position: "top-right",
        autoClose: process.env.API_TOAST_TIME,
        hideProgressBar: false,
        closeOnClick: false,
        pauseOnHover: false,
        draggable: false
      });
  }

  ToastError(msg){
      toast.error(msg, {
        position: "top-right",
        autoClose: process.env.API_TOAST_TIME,
        hideProgressBar: false,
        closeOnClick: false,
        pauseOnHover: false,
        draggable: false
      });
  }
  // toast hide show method

  componentDidMount() {

    /* Role Management */
     // console.log('Role Store volunteer_can');
     /*var getrole = SystemHelpers.GetRole();
     let volunteer_can = getrole.volunteer_can;
     this.setState({ role_volunteer_can: volunteer_can });
     // console.log(volunteer_can);*/

    // console.log(this.props.volunteer_can);
    let volunteer_can = this.props.volunteer_can;
    this.setState({ role_volunteer_can: this.props.volunteer_can });
    /* Role Management */



    // console.log("DidMount Volunteer");
    // console.log(this.state.all_data);

    //this.GetUserRegistrationDetail();

    this.setState({ refereedName: this.state.all_data.refereName });
    this.setState({ volunteeringstatus: this.state.all_data.volunteeringstatus });
    this.setState({ volunteeringtype: this.state.all_data.volunteeringtype }); 
    this.setState({ volunteeringhours: this.state.all_data.volunteeringhours });

    this.setState({ volunteerStatusList: this.state.all_data.volunteerStatusList });
    this.setState({ volunteerTypeList: this.state.all_data.volunteerTypeList }); 
    this.setState({ volunteerHoursList: this.state.all_data.volunteerHoursList });

    this.setState({ EditrefereedName: this.state.all_data.refereName });
    this.setState({ Editvolunteeringstatus: this.state.all_data.volunteeringstatusId }); 
    this.setState({ Editvolunteeringtype: this.state.all_data.volunteeringtypeId });
    this.setState({ Editvolunteeringhours: this.state.all_data.volunteeringhoursId });
  }

  TabClickOnLoadVolunteer = () => e => {
    //debugger;
    e.preventDefault();

    this.GetUserRegistrationDetail();
  }

  push_func(){
    this.props.history.push("/");
    this.hideLoader();
  }

  SessionOut(){
    this.showLoader();
  
    localStorage.removeItem("token");
    localStorage.removeItem("contactId");
    localStorage.removeItem("eMailAddress");
    localStorage.removeItem("fullName");

    //this.ToastError('Session Timeout');
    
    setTimeout( () => {this.push_func()}, 5000);
    
    return null;
  }

 
  GetUserRegistrationDetail(){
    this.showLoader();

    var getrole = SystemHelpers.GetRole();
    let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
    
    var url=process.env.API_API_URL+'GetUserRegistrationDetail?rolePriorityId='+hierarchyId+'&contactId='+localStorage.getItem("contactId");
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        // console.log("responseJson GetUserRegistrationDetail");
        // console.log(data);
        // console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            if(data.data != null){
              //this.setState({ ListGrid: data.data.userEmergencyContactViews });
              this.setState({ userTypeList: data.data.userTypeList });
              this.setState({ roleList: data.data.roleList });
              //var location_list=data.data.locationList;
              //this.setState({ locationList: this.location_list_fun(location_list) }); 
            }
            
            
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('GetUserRegistrationDetail error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }

  //
  GetProfile(){
    // console.log(localStorage.getItem("token"));
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserBasicInfoById?contactId='+this.state.staffContactID;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        // console.log("responseJson GetUserBasicInfoById");
        // console.log(data);
        // console.log(data.data.userRole);
        // debugger;
        
         
        if (data.responseType === "1") {
            this.setState({ all_data: data.data});
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                this.SessionOut();
              }else{
                this.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('GetUserBasicInfoById error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }
  //

  // Volunteer model Update api
  UpdateRecord = () => e => {  
      e.preventDefault();  
      
      let step1Errors = {};
      if (this.state["EditrefereedName"] == '') {
        step1Errors["EditrefereedName"] = "Please Enter Referred by"
      }
      
      if (this.state["Editvolunteeringstatus"] == '' || this.state["Editvolunteeringstatus"] == null) {
        step1Errors["Editvolunteeringstatus"] = "Please Select Volunteering status"
      }
      if (this.state["Editvolunteeringtype"] == '' || this.state["Editvolunteeringtype"] == null) {
        step1Errors["Editvolunteeringtype"] = "Please Select Volunteering type"
      }

      if (this.state["Editvolunteeringhours"] == '' || this.state["Editvolunteeringhours"] == null) {
        step1Errors["Editvolunteeringhours"] = "Please Select Volunteering hours"
      }
      
      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      this.showLoader();
      
      
      let User_extended_profile_PHHS_EmpInfo = {
        referBy: this.state["EditrefereedName"],
        volunteerStatus: this.state["Editvolunteeringstatus"],
        volunteerType: this.state["Editvolunteeringtype"],
        volunteerHours: this.state["Editvolunteeringhours"],
        changeBy: localStorage.getItem("fullName"),
      };

      let bodyarray = {};
      bodyarray["contactId"] = this.state.staffContactID;
      bodyarray["volunteerModel"] = User_extended_profile_PHHS_EmpInfo;
      bodyarray["userName"] = this.state.staffContactFullname;
      
      var url=process.env.API_API_URL+'UpdateUserVolunteersInfo';
      fetch(url, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
        if (data.responseType === "1") {
          this.GetProfile();  
          this.ToastSuccess(data.responseMessge);
          $( "#close_btn_Volunteer" ).trigger( "click" );
        }
        else{
          this.ToastError(data.responseMessge);
        }
        this.hideLoader();
      })
      .catch(error => {
        console.log('UpdateUserVolunteersInfo error');
        console.log(error);
        this.props.history.push("/error-500");
      });
  }
  // Volunteer model Update api

  ClearRecord = ()=> e => {
    e.preventDefault();

    /* Role Management */
     // console.log('Role Store volunteer_can');
     var getrole = SystemHelpers.GetRole();
     let volunteer_can = getrole.volunteer_can;
     this.setState({ role_volunteer_can: volunteer_can });
     // console.log(volunteer_can);
    /* Role Management */

    this.setState({ errormsg: '' });

    this.GetUserRegistrationDetail();

    this.setState({ refereedName: this.state.all_data.refereName });
    this.setState({ volunteeringstatus: this.state.all_data.volunteeringstatus });
    this.setState({ volunteeringtype: this.state.all_data.volunteeringtype }); 
    this.setState({ volunteeringhours: this.state.all_data.volunteeringhours });

    this.setState({ volunteerStatusList: this.state.all_data.volunteerStatusList });
    this.setState({ volunteerTypeList: this.state.all_data.volunteerTypeList }); 
    this.setState({ volunteerHoursList: this.state.all_data.volunteerHoursList });

    this.setState({ EditrefereedName: this.state.all_data.refereName });
    this.setState({ Editvolunteeringstatus: this.state.all_data.volunteeringstatus }); 
    this.setState({ Editvolunteeringtype: this.state.all_data.volunteeringtype });
    this.setState({ Editvolunteeringhours: this.state.all_data.volunteeringhours });
  }

   render() {
     
      return (
        
          <div className="row">
            {(this.state.loading) ? <Loader /> : null}
            <div className="col-md-12 d-flex">
              <div className="card profile-box flex-fill">

                <div className="row">
                  <button className="btn btn-primary submit-btn pk-profiletab-refreshbtn-hide" id="TabClickOnLoadVolunteer" onClick={this.TabClickOnLoadVolunteer()}>Refresh</button>
                </div>

                <div className="card-body">
                  {this.state.role_volunteer_can.volunteer_can_update == true ? 
                    <h3 className="card-title">Volunteer<a href="#" className="edit-icon" data-toggle="modal" data-target="#Volunteer_modal"><i className="fa fa-pencil" /></a></h3>
                    : <h3 className="card-title">Volunteer <a href="#" className="phss-lock"><i className="fa fa-lock" /></a></h3>
                  }
                  <ul className="personal-info">
                    <li>
                      <div className="title">Referred by</div>
                      {this.state.all_data.refereName == '' || this.state.all_data.refereName == null? <div className="text hide-font">None</div> : <div className="text">{this.state.all_data.refereName}</div> }
                    </li>
                    <li>
                      <div className="title">Volunteering status</div>
                      {this.state.all_data.volunteeringstatus == '' || this.state.all_data.volunteeringstatus == null? <div className="text hide-font">None</div> : <div className="text">{this.state.all_data.volunteeringstatus}</div> }
                    </li>
                    <li>
                      <div className="title">Volunteering type</div>
                      {this.state.all_data.volunteeringtype == '' || this.state.all_data.volunteeringtype == null? <div className="text hide-font">None</div> : <div className="text">{this.state.all_data.volunteeringtype}</div> }
                    </li>
                    <li>
                      <div className="title">Volunteering hours</div>
                      {this.state.all_data.volunteeringhours == '' || this.state.all_data.volunteeringhours == null? <div className="text hide-font">None</div> : <div className="text">{this.state.all_data.volunteeringhours}</div> }
                    </li>
                  </ul>
                </div>
              </div>
            </div>
             {/* Volunteer Modal */}
            <div id="Volunteer_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Volunteer</h5>
                    <button type="button" className="close" id="close_btn_Volunteer" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()} >
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Referred by</label>
                                <input className="form-control" type="text" value={this.state.EditrefereedName} onChange={this.handleChange('EditrefereedName')} />
                                <span className="form-text error-font-color">{this.state.errormsg["EditrefereedName"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Volunteering status<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.Editvolunteeringstatus} onChange={this.handleChange('Editvolunteeringstatus')}>
                                  <option value="">-</option>
                                  {this.state.volunteerStatusList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.id}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["Editvolunteeringstatus"]}</span> 
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Volunteering type<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.Editvolunteeringtype} onChange={this.handleChange('Editvolunteeringtype')}>
                                  <option value="">-</option>
                                  {this.state.volunteerTypeList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.id} >{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["Editvolunteeringtype"]}</span> 
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Volunteering hours<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.Editvolunteeringhours} onChange={this.handleChange('Editvolunteeringhours')}>
                                  <option value="">-</option>
                                  {this.state.volunteerHoursList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.id} >{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["Editvolunteeringhours"]}</span> 
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" type="submit" onClick={this.UpdateRecord({})} >Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* /Volunteer Modal */}
          </div>
        
      );
   }
}

export default PhssEmploymentInformation;
